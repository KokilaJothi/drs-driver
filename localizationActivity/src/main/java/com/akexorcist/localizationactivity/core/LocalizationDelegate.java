package com.akexorcist.localizationactivity.core;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by Aleksander Mielczarek on 03.04.2016.
 */


/**
 * Use LocalizationActivityDelegate
 */

@Deprecated
public class LocalizationDelegate extends LocalizationActivityDelegate {
    public LocalizationDelegate(AppCompatActivity activity) {
        super(activity);
    }
}
