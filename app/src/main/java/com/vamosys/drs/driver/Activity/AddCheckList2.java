package com.vamosys.drs.driver.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.vamosys.drs.driver.ApiClient.ApiClient;
import com.vamosys.drs.driver.ApiClient.ApiInterface;
import com.vamosys.drs.driver.ApiClient.Const;
import com.vamosys.drs.driver.CheckListPereference;
import com.vamosys.drs.driver.HomeBottomNavigation;
import com.vamosys.drs.driver.R;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddCheckList2 extends AppCompatActivity {
    //lowbeam
    Button lowok, lownotok, lowrewark;

    //signals
    Button signalok, signalnotok, signalrewards;

    //lights
    Button lightok, lightnotok, lightrewards;

    //door
    Button doorok, doornotok, doorrewards;

    //windows
    Button windowok, windownotok, windowrewards;

    //radio
    Button radiook, radionotok, radiorewards;

    //horn
    Button hornok, hornnotok, hornrewards;

    //tires
    Button tiresok, tiresotok, tiresrewards;

    Button submit;

    CheckListPereference pereference;

    ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_check_list2);
        apiInterface= ApiClient.getClient().create(ApiInterface.class);

        pereference =new CheckListPereference(AddCheckList2.this);
        submit= findViewById(R.id.btn_checklist);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call<ResponseBody> call=apiInterface.addVehicleCheckList(Const.VehicleId,pereference.getSateft_belt(),pereference.getBrake()
                        ,pereference.getEngine(),pereference.getTransmission(),pereference.getGreecePacking(),pereference.getWipers(),
                        pereference.getHighbeam(),pereference.getLowbeam(),pereference.getTurnSignal(),pereference.getBreakLight(),
                        pereference.getDoors(),pereference.getWindows(),pereference.getRadio(),pereference.getHorn(),pereference.getTires(),
                        "","",""
                        ,"","","",
                        "","","","",
                        "","",Const.driverId,"","");
                Log.d("response","url checkList  "+call.request().url().toString());
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            String result=response.body().string();
                            Log.d("response","checkList "+result);
                            pereference.setIsCheckListAdded(true);
                            startActivity(new Intent(AddCheckList2.this, HomeBottomNavigation.class));

                        }
                        catch (Exception e){
                            Log.d("response","checkList "+e.getMessage());

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.d("response","checkList "+t.getMessage());

                    }
                });

            }
        });

        lowok = findViewById(R.id.btn_lowok);
        lownotok = findViewById(R.id.btn_lownotok);
        lowrewark = findViewById(R.id.btn_lowrem);

        signalok = findViewById(R.id.btn_signalok);
        signalnotok = findViewById(R.id.btn_signalnotok);
        signalrewards = findViewById(R.id.btn_signalrem);

        lightok = findViewById(R.id.btn_lightok);
        lightnotok = findViewById(R.id.btn_lightnotok);
        lightrewards = findViewById(R.id.btn_lightrem);

        doorok = findViewById(R.id.btn_doorok);
        doornotok = findViewById(R.id.btn_doornotok);
        doorrewards = findViewById(R.id.btn_doorrem);

        windowok = findViewById(R.id.btn_windowok);
        windownotok = findViewById(R.id.btn_windownotok);
        windowrewards = findViewById(R.id.btn_windowrem);

        radiook = findViewById(R.id.btn_radiook);
        radionotok = findViewById(R.id.btn_radionotok);
        radiorewards = findViewById(R.id.btn_radiorem);

        hornok = findViewById(R.id.btn_hornok);
        hornnotok = findViewById(R.id.btn_hornnotok);
        hornrewards = findViewById(R.id.btn_hornrem);

        tiresok = findViewById(R.id.btn_tiresok);
        tiresotok = findViewById(R.id.btn_tiresnotok);
        tiresrewards = findViewById(R.id.btn_tiresrem);
        setSelectedData();

        lowok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lowok.setBackgroundResource(R.drawable.curve_pink);
                lowok.setTextColor(Color.parseColor("#C41027"));

                lownotok.setBackgroundResource(R.drawable.curve_grey);
                lownotok.setTextColor(Color.parseColor("#000000"));

                lowrewark.setBackgroundResource(R.drawable.curve_grey);
                lowrewark.setTextColor(Color.parseColor("#000000"));

                pereference.setLowbeam("OK:-");
            }
        });


        lownotok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lownotok.setBackgroundResource(R.drawable.curve_pink);
                lownotok.setTextColor(Color.parseColor("#C41027"));

                lowok.setBackgroundResource(R.drawable.curve_grey);
                lowok.setTextColor(Color.parseColor("#000000"));

                lowrewark.setBackgroundResource(R.drawable.curve_grey);
                lowrewark.setTextColor(Color.parseColor("#000000"));

                pereference.setLowbeam("NOTOK:-");
            }
        });

        lowrewark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lowrewark.setBackgroundResource(R.drawable.curve_pink);
                lowrewark.setTextColor(Color.parseColor("#C41027"));

                lowok.setBackgroundResource(R.drawable.curve_grey);
                lowok.setTextColor(Color.parseColor("#000000"));

                lownotok.setBackgroundResource(R.drawable.curve_grey);
                lownotok.setTextColor(Color.parseColor("#000000"));

                pereference.setLowbeam("-:Remarks");
            }
        });

        signalok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signalok.setBackgroundResource(R.drawable.curve_pink);
                signalok.setTextColor(Color.parseColor("#C41027"));

                signalnotok.setBackgroundResource(R.drawable.curve_grey);
                signalnotok.setTextColor(Color.parseColor("#000000"));

                signalrewards.setBackgroundResource(R.drawable.curve_grey);
                signalrewards.setTextColor(Color.parseColor("#000000"));

                pereference.setTurnSignal("OK:-");
            }
        });


        signalnotok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signalnotok.setBackgroundResource(R.drawable.curve_pink);
                signalnotok.setTextColor(Color.parseColor("#C41027"));

                signalok.setBackgroundResource(R.drawable.curve_grey);
                signalok.setTextColor(Color.parseColor("#000000"));

                signalrewards.setBackgroundResource(R.drawable.curve_grey);
                signalrewards.setTextColor(Color.parseColor("#000000"));

                pereference.setTurnSignal("NOTOK:-");
            }
        });

        signalrewards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signalrewards.setBackgroundResource(R.drawable.curve_pink);
                signalrewards.setTextColor(Color.parseColor("#C41027"));

                signalok.setBackgroundResource(R.drawable.curve_grey);
                signalok.setTextColor(Color.parseColor("#000000"));

                signalnotok.setBackgroundResource(R.drawable.curve_grey);
                signalnotok.setTextColor(Color.parseColor("#000000"));

                pereference.setTurnSignal("-:Remarks");
            }
        });


        lightok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lightok.setBackgroundResource(R.drawable.curve_pink);
                lightok.setTextColor(Color.parseColor("#C41027"));

                lightnotok.setBackgroundResource(R.drawable.curve_grey);
                lightnotok.setTextColor(Color.parseColor("#000000"));

                lightrewards.setBackgroundResource(R.drawable.curve_grey);
                lightrewards.setTextColor(Color.parseColor("#000000"));

                pereference.setBreakLight("OK:-");
            }
        });

        lightnotok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lightnotok.setBackgroundResource(R.drawable.curve_pink);
                lightnotok.setTextColor(Color.parseColor("#C41027"));

                lightok.setBackgroundResource(R.drawable.curve_grey);
                lightok.setTextColor(Color.parseColor("#000000"));

                lightrewards.setBackgroundResource(R.drawable.curve_grey);
                lightrewards.setTextColor(Color.parseColor("#000000"));


                pereference.setBreakLight("NOTOK:-");
            }
        });

        lightrewards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                lightrewards.setBackgroundResource(R.drawable.curve_pink);
                lightrewards.setTextColor(Color.parseColor("#C41027"));

                lightok.setBackgroundResource(R.drawable.curve_grey);
                lightok.setTextColor(Color.parseColor("#000000"));

                lightnotok.setBackgroundResource(R.drawable.curve_grey);
                lightnotok.setTextColor(Color.parseColor("#000000"));

                pereference.setBreakLight("-:Remarks");
            }
        });

        doorok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doorok.setBackgroundResource(R.drawable.curve_pink);
                doorok.setTextColor(Color.parseColor("#C41027"));

                doornotok.setBackgroundResource(R.drawable.curve_grey);
                doornotok.setTextColor(Color.parseColor("#000000"));

                doorrewards.setBackgroundResource(R.drawable.curve_grey);
                doorrewards.setTextColor(Color.parseColor("#000000"));

                pereference.setDoors("OK:-");
            }
        });

        doornotok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doornotok.setBackgroundResource(R.drawable.curve_pink);
                doornotok.setTextColor(Color.parseColor("#C41027"));

                doorok.setBackgroundResource(R.drawable.curve_grey);
                doorok.setTextColor(Color.parseColor("#000000"));

                doorrewards.setBackgroundResource(R.drawable.curve_grey);
                doorrewards.setTextColor(Color.parseColor("#000000"));
                pereference.setDoors("NOTOK:-");
            }
        });

        doorrewards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doorrewards.setBackgroundResource(R.drawable.curve_pink);
                doorrewards.setTextColor(Color.parseColor("#C41027"));

                doorok.setBackgroundResource(R.drawable.curve_grey);
                doorok.setTextColor(Color.parseColor("#000000"));

                doornotok.setBackgroundResource(R.drawable.curve_grey);
                doornotok.setTextColor(Color.parseColor("#000000"));

                pereference.setDoors("-:Remarks");
            }
        });

        windowok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                windowok.setBackgroundResource(R.drawable.curve_pink);
                windowok.setTextColor(Color.parseColor("#C41027"));

                windownotok.setBackgroundResource(R.drawable.curve_grey);
                windownotok.setTextColor(Color.parseColor("#000000"));

                windowrewards.setBackgroundResource(R.drawable.curve_grey);
                windowrewards.setTextColor(Color.parseColor("#000000"));

                pereference.setWindows("OK:-");
            }
        });

        windownotok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                windownotok.setBackgroundResource(R.drawable.curve_pink);
                windownotok.setTextColor(Color.parseColor("#C41027"));

                windowok.setBackgroundResource(R.drawable.curve_grey);
                windowok.setTextColor(Color.parseColor("#000000"));

                windowrewards.setBackgroundResource(R.drawable.curve_grey);
                windowrewards.setTextColor(Color.parseColor("#000000"));
                pereference.setWindows("NOTOK:-");
            }
        });

        windowrewards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                windowrewards.setBackgroundResource(R.drawable.curve_pink);
                windowrewards.setTextColor(Color.parseColor("#C41027"));

                windowok.setBackgroundResource(R.drawable.curve_grey);
                windowok.setTextColor(Color.parseColor("#000000"));

                windownotok.setBackgroundResource(R.drawable.curve_grey);
                windownotok.setTextColor(Color.parseColor("#000000"));

                pereference.setWindows("-:Remarks");
            }
        });


        radiook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radiook.setBackgroundResource(R.drawable.curve_pink);
                radiook.setTextColor(Color.parseColor("#C41027"));

                radionotok.setBackgroundResource(R.drawable.curve_grey);
                radionotok.setTextColor(Color.parseColor("#000000"));

                radiorewards.setBackgroundResource(R.drawable.curve_grey);
                radiorewards.setTextColor(Color.parseColor("#000000"));

                pereference.setRadio("OK:-");
            }
        });

        radionotok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radionotok.setBackgroundResource(R.drawable.curve_pink);
                radionotok.setTextColor(Color.parseColor("#C41027"));

                radiook.setBackgroundResource(R.drawable.curve_grey);
                radiook.setTextColor(Color.parseColor("#000000"));

                radiorewards.setBackgroundResource(R.drawable.curve_grey);
                radiorewards.setTextColor(Color.parseColor("#000000"));

                pereference.setRadio("NOTOK:-");
            }
        });
        radiorewards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radiorewards.setBackgroundResource(R.drawable.curve_pink);
                radiorewards.setTextColor(Color.parseColor("#C41027"));

                radiook.setBackgroundResource(R.drawable.curve_grey);
                radiook.setTextColor(Color.parseColor("#000000"));

                radionotok.setBackgroundResource(R.drawable.curve_grey);
                radionotok.setTextColor(Color.parseColor("#000000"));

                pereference.setRadio("-:Remarks");
            }
        });

        hornok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hornok.setBackgroundResource(R.drawable.curve_pink);
                hornok.setTextColor(Color.parseColor("#C41027"));

                hornnotok.setBackgroundResource(R.drawable.curve_grey);
                hornnotok.setTextColor(Color.parseColor("#000000"));

                hornrewards.setBackgroundResource(R.drawable.curve_grey);
                hornrewards.setTextColor(Color.parseColor("#000000"));

                pereference.setHorn("OK:-");
            }
        });

        hornnotok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hornnotok.setBackgroundResource(R.drawable.curve_pink);
                hornnotok.setTextColor(Color.parseColor("#C41027"));

                hornok.setBackgroundResource(R.drawable.curve_grey);
                hornok.setTextColor(Color.parseColor("#000000"));

                hornrewards.setBackgroundResource(R.drawable.curve_grey);
                hornrewards.setTextColor(Color.parseColor("#000000"));

                pereference.setHorn("NOTOK:-");
            }
        });

        hornrewards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hornrewards.setBackgroundResource(R.drawable.curve_pink);
                hornrewards.setTextColor(Color.parseColor("#C41027"));

                hornok.setBackgroundResource(R.drawable.curve_grey);
                hornok.setTextColor(Color.parseColor("#000000"));

                hornnotok.setBackgroundResource(R.drawable.curve_grey);
                hornnotok.setTextColor(Color.parseColor("#000000"));

                pereference.setHorn("-:Remarks");
            }
        });

        tiresok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tiresok.setBackgroundResource(R.drawable.curve_pink);
                tiresok.setTextColor(Color.parseColor("#C41027"));

                tiresotok.setBackgroundResource(R.drawable.curve_grey);
                tiresotok.setTextColor(Color.parseColor("#000000"));

                tiresrewards.setBackgroundResource(R.drawable.curve_grey);
                tiresrewards.setTextColor(Color.parseColor("#000000"));
                pereference.setTires("OK:-");
            }
        });

        tiresotok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tiresotok.setBackgroundResource(R.drawable.curve_pink);
                tiresotok.setTextColor(Color.parseColor("#C41027"));

                tiresok.setBackgroundResource(R.drawable.curve_grey);
                tiresok.setTextColor(Color.parseColor("#000000"));

                tiresrewards.setBackgroundResource(R.drawable.curve_grey);
                tiresrewards.setTextColor(Color.parseColor("#000000"));
                pereference.setTires("NOTOK:-");
            }
        });


        tiresrewards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tiresrewards.setBackgroundResource(R.drawable.curve_pink);
                tiresrewards.setTextColor(Color.parseColor("#C41027"));

                tiresok.setBackgroundResource(R.drawable.curve_grey);
                tiresok.setTextColor(Color.parseColor("#000000"));

                tiresotok.setBackgroundResource(R.drawable.curve_grey);
                tiresotok.setTextColor(Color.parseColor("#000000"));

                pereference.setTires("-:Remarks");
            }
        });

    }
    private void setSelectedData(){
        setBackgroundButton(lowok,lownotok,lowrewark,pereference.getLowbeam());
        setBackgroundButton(signalok,signalnotok,signalrewards,pereference.getTurnSignal());
        setBackgroundButton(lightok,lightnotok,lightrewards,pereference.getBreakLight());
        setBackgroundButton(doorok,doornotok,doorrewards,pereference.getDoors());
        setBackgroundButton(windowok,windownotok,windowrewards,pereference.getWindows());
        setBackgroundButton(radiook,radionotok,radiorewards,pereference.getRadio());
        setBackgroundButton(hornok,hornnotok,hornrewards,pereference.getHorn());
        setBackgroundButton(tiresok,tiresotok,tiresrewards,pereference.getTires());

    }
    private void setBackgroundButton(Button btn1,Button btn2,Button btn3, String click_val){
        if(click_val.equals("OK:-")){
            btn1.setBackgroundResource(R.drawable.curve_pink);
            btn2.setBackgroundResource(R.drawable.curve_grey);
            btn3.setBackgroundResource(R.drawable.curve_grey);
        }
        else if(click_val.equals("NOTOK:-")){
            btn1.setBackgroundResource(R.drawable.curve_grey);
            btn2.setBackgroundResource(R.drawable.curve_pink);
            btn3.setBackgroundResource(R.drawable.curve_grey);
        }
        else if(click_val.equals("-:Remarks")) {
            btn1.setBackgroundResource(R.drawable.curve_grey);
            btn2.setBackgroundResource(R.drawable.curve_grey);
            btn3.setBackgroundResource(R.drawable.curve_pink);
        }

    }
}
