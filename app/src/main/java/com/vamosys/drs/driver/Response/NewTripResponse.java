package com.vamosys.drs.driver.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewTripResponse {

    @SerializedName("vehicleName")
    @Expose
    private String vehicleName;
    @SerializedName("seqNum")
    @Expose
    private long seqNum;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("locationType")
    @Expose
    private String locationType;
    @SerializedName("pickupLocation")
    @Expose
    private String pickupLocation;
    @SerializedName("pickupDateTime")
    @Expose
    private long pickupDateTime;
    @SerializedName("nextDestination")
    @Expose
    private String nextDestination;
    @SerializedName("createdDateTime")
    @Expose
    private long createdDateTime;
    @SerializedName("contactPerson")
    @Expose
    private String contactPerson;
    @SerializedName("driverId")
    @Expose
    private String driverId;
    @SerializedName("statusOfDriver")
    @Expose
    private String statusOfDriver;

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public long getSeqNum() {
        return seqNum;
    }

    public void setSeqNum(long seqNum) {
        this.seqNum = seqNum;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public String getPickupLocation() {
        return pickupLocation;
    }

    public void setPickupLocation(String pickupLocation) {
        this.pickupLocation = pickupLocation;
    }

    public long getPickupDateTime() {
        return pickupDateTime;
    }

    public void setPickupDateTime(long pickupDateTime) {
        this.pickupDateTime = pickupDateTime;
    }

    public String getNextDestination() {
        return nextDestination;
    }

    public void setNextDestination(String nextDestination) {
        this.nextDestination = nextDestination;
    }

    public long getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(long createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getStatusOfDriver() {
        return statusOfDriver;
    }

    public void setStatusOfDriver(String statusOfDriver) {
        this.statusOfDriver = statusOfDriver;
    }

}