package com.vamosys.drs.driver.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallState;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.gson.JsonObject;
import com.vamosys.drs.driver.ApiClient.ApiClient;
import com.vamosys.drs.driver.ApiClient.ApiInterface;
import com.vamosys.drs.driver.ApiClient.Const;
import com.vamosys.drs.driver.AppPreference;
import com.vamosys.drs.driver.HomeBottomNavigation;
import com.vamosys.drs.driver.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.hbb20.CountryCodePicker;
import com.vamosys.drs.driver.Response.SpinnerHub;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SigninActivity extends LocalizationActivity {
    LinearLayout btn_otp;
    String contact;
    EditText edt_phone;
    private String mobilenumber,phonenumber;
    private String verificationid;
    private PhoneNumberUtil phoneUtil;
    FirebaseFirestore db;
    private int countryCodeInt;
    CountryCodePicker codePicker;
    private String isexit = "";
    private boolean userExist;
    String isexist,isRole;
    AppPreference appPreference;
    private SharedPreferences preferences;
    SharedPreferences.Editor editor;
    ProgressDialog progressDialog;
    ApiInterface apiInterface;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        getHub();
        preferences = getSharedPreferences("login_session", Context.MODE_PRIVATE);
        progressDialog = new ProgressDialog(SigninActivity.this);
            progressDialog.setMessage("Loading ....");
            progressDialog.setCancelable(false);
        FirebaseApp.initializeApp(this);
        appPreference=new AppPreference(this);
        db = FirebaseFirestore.getInstance();
        btn_otp=findViewById(R.id.lyt_login);
        edt_phone=findViewById(R.id.edt_phone);
        codePicker = findViewById(R.id.ccp);
        phoneUtil = PhoneNumberUtil.getInstance();
        codePicker.setCcpClickable(false);

        if (appPreference.getLangName().equalsIgnoreCase("english")) {
            setLanguage("en");
        }
        if (appPreference.getLangName().equalsIgnoreCase("hindi")) {
            setLanguage("hi");
        }
        edt_phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                mobilenumber = charSequence.toString();
                countryCodeInt = codePicker.getSelectedCountryCodeAsInt();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        edt_phone.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                Log.d("mobilenumber",""+v.getText().toString());
                String countrycode =""+codePicker.getDefaultCountryCode();
                Log.d("countrycode",""+countrycode);
                countryCodeInt = codePicker.getSelectedCountryCodeAsInt();
                if (mobilenumber!=null) {
                    //  if (isPhoneNumberValid(mobilenumber, "+91")) {
                    //  Toast.makeText(LoginActivity.this, "Valid success", Toast.LENGTH_SHORT).show();
                    Log.d("ismobilenumb", "" + mobilenumber);
                    //  if(checkMble(mobilenumber)){


                    if (mobilenumber.length() == 10) {
                        if (isPhoneNumberValid(mobilenumber, "+91")) {
                            phonenumber = countrycode + mobilenumber;
                            checkMobileNumberExists();
                        }
                        else {
                           // Toast.makeText(getApplicationContext(),"Not Valid",Toast.LENGTH_SHORT).show();
                        }
                    }

                    else {
                        edt_phone.setError(getText(R.string.evalidpho));
                    }
                }


                else {
                    edt_phone.setError(getText(R.string.evalidpho));
                }

                return false;
            }
        });


        btn_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String countrycode =""+codePicker.getDefaultCountryCode();
                Log.d("countrycode",""+countrycode);
                countryCodeInt = codePicker.getSelectedCountryCodeAsInt();


                if (mobilenumber!=null) {
                  //  if (isPhoneNumberValid(mobilenumber, "+91")) {
                        //  Toast.makeText(LoginActivity.this, "Valid success", Toast.LENGTH_SHORT).show();
                        Log.d("ismobilenumb", "" + mobilenumber);
                      //  if(checkMble(mobilenumber)){


                        if (mobilenumber.length() == 10) {
                            if (isPhoneNumberValid(mobilenumber, "+91")) {
                                phonenumber = countrycode + mobilenumber;
                                checkMobileNumberExists();
                            }
                            else {
                              //  Toast.makeText(getApplicationContext(),"Not Valid",Toast.LENGTH_SHORT).show();
                            }
                        }

                        else {
                            edt_phone.setError(getText(R.string.evalidpho));
                        }
                    }


                else {
                    edt_phone.setError(getText(R.string.evalidpho));
                }
            }
        });
    }

    private void savenumber(String mobilenumber) {
        Map< String, Object > newnumber = new HashMap< >();
        newnumber.put("mobilenumber", mobilenumber);
        CollectionReference user = db.collection("Users");

        user.add(newnumber).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                Toast.makeText(SigninActivity.this, "User added", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(SigninActivity.this, "User not added", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean validate() {
        Boolean check=true;
        if(edt_phone.getText().toString()!=null && edt_phone.getText().length()>=10){
            contact=edt_phone.getText().toString();
            if (Patterns.PHONE.matcher(contact).matches()) {
                check=true;
            }
            else{
                contact=null;
                check=false;
                edt_phone.setError(getText(R.string.evalidpho));
                return check;
            }
            return check;
        }
        else{
            check=false;
            edt_phone.setError(getText(R.string.evalidpho));
            return check;
        }
    }





    public boolean isPhoneNumberValid(String phoneNumber,String countrycode)
    {


        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
        String isocode = phoneNumberUtil.getRegionCodeForCountryCode(Integer.parseInt(countrycode));
        Phonenumber.PhoneNumber number = null;
        try {
            //String formattedNumber = phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
            //   String pnE164 = phoneNumberUtil.format(number, PhoneNumberUtil.PhoneNumberFormat.E164);

            number = phoneNumberUtil.parse(phoneNumber,isocode);
        }catch (NumberParseException e){

        }

        boolean isvalid = phoneNumberUtil.isValidNumber(number);
        if (isvalid){
//                String interformat = phoneNumberUtil.format(number,PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);
            String interformat = phoneNumberUtil.format(number, PhoneNumberUtil.PhoneNumberFormat.E164);
            return true;
        }else {
            return false;
        }
    }
    private void checkMobileNumberExists(){
        progressDialog.show();
        CollectionReference reference = db.collection("Users");
        Query usernamequery = reference.whereEqualTo("mobilenumber",mobilenumber);
        usernamequery.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    Log.d("getsms","addOnComplete sucess");
                    for (DocumentSnapshot document : task.getResult()) {
                        Log.d("getsms","for "+document.getString("mobilenumber"));
                        if (document.exists()) {
                            Log.d("getsms","document exists");
                            String userName = document.getString("mobilenumber");
                            String role = document.getString("Role");
                            String UUid=document.getString("Uuid");
                            if (role!=null) {
                                if (role.equalsIgnoreCase("FleetOwner")) {
                                    isRole = "FleetOwner";
                                } else {
                                    isRole = "Driver";
                                }
                            }
//                            System.out.println("userNamnum"+userName);
//                            Const.UserExists=true;
//                            userExist=true;
//
//                           isexist= "Already Exists";
                        }
                        else {
                            Log.d("getsms","document not exists");
                        }
                    }
                    callIntent();
                }
                else{
                    Log.d("getsms","addOnComplete fail");
                    System.out.println("Error getting documents: "+task.getException());
                    callIntent();
                }
            }
        });
    }
    private void callIntent(){
        Log.d("getsms","userin "+ Const.UserExists);
        if (isRole!=null){
            if (isRole.equalsIgnoreCase("FleetOwner")){
                Toast.makeText(this, getResources().getString(R.string.AlreadyRegistered), Toast.LENGTH_SHORT).show();
                if(progressDialog.isShowing()){
                    progressDialog.cancel();
                }

            }
            else{
                String num=phonenumber;
                registerMblNum(num.replace("+",""));
            }
        }
        else {
            String num=phonenumber;
            registerMblNum(num.replace("+",""));
        }


    }




private Boolean checkMble(String MobileNumber){
        if(MobileNumber.contains("+")&&MobileNumber.length()>10){
            return false;
        }
        else{
            return true;
        }
}

private void registerMblNum(String mobilenumbers){
    ApiInterface apiInterface= ApiClient.getClient().create(ApiInterface.class);
    Call<ResponseBody> call = apiInterface.registerMobileNum(mobilenumbers);
    Log.d("response","response url "+call.request().url().toString());
    call.enqueue(new Callback<ResponseBody>() {
        @Override
        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            Log.d("response","response  "+response.body());
            try {
                String result=response.body().string();
                JSONObject jsonObject= new JSONObject(result.trim());
                String status=jsonObject.getString("status");
                String uuid=jsonObject.getString("uuid");
                if(status.equalsIgnoreCase("NEW USER")){
                    Log.d("response","status  "+status);
                    appPreference.setUuid(uuid);

                    Intent i=new Intent(SigninActivity.this, RegistrationActivity.class);
                    i.putExtra("phonenumber",phonenumber);
                    i.putExtra("mobilenumber",mobilenumber);
                    i.putExtra("userExist",userExist);
                    i.putExtra("isexist",isexist);
                    startActivity(i);
                    if(progressDialog.isShowing()){
                        progressDialog.cancel();
                    }
                }
                else if(status.equalsIgnoreCase("OLD USER")){
                    if(isRole!=null){
                        Log.d("response","status  "+status);
                        appPreference.setUuid(uuid);
                        editor=preferences.edit();
                        editor.putBoolean("is_loggin",true);
                        editor.putString("mobilenumber",mobilenumber);
                        editor.commit();

                        Intent i=new Intent(SigninActivity.this, HomeBottomNavigation.class);
                        i.putExtra("phonenumber",phonenumber);
                        i.putExtra("mobilenumber",mobilenumber);
                        i.putExtra("userExist",userExist);
                        i.putExtra("isexist",isexist);
                        startActivity(i);
                        if(progressDialog.isShowing()){
                            progressDialog.cancel();
                        }
                    }
                    else {
                        Log.d("response","status  "+status);
                        appPreference.setUuid(uuid);

                        Intent i=new Intent(SigninActivity.this, RegistrationActivity.class);
                        i.putExtra("phonenumber",phonenumber);
                        i.putExtra("mobilenumber",mobilenumber);
                        i.putExtra("userExist",userExist);
                        i.putExtra("isexist",isexist);
                        startActivity(i);
                        if(progressDialog.isShowing()){
                            progressDialog.cancel();
                        }
                    }

                }
            }
            catch (Exception e){
                Log.d("response","response  "+e.getMessage());
                if(progressDialog.isShowing()){
                    progressDialog.cancel();
                }
            }
        }

        @Override
        public void onFailure(Call<ResponseBody> call, Throwable t) {
            if(progressDialog.isShowing()){
                progressDialog.cancel();
            }
            Log.d("response","response  "+t.getMessage());
        }
    });
}

    private void getHub() {

        final Call<List<SpinnerHub>> spinnerHubCall = apiInterface.getSpinnerHub();
        //final ProgressDialog progressDialog;
        //progressDialog = new ProgressDialog(RegistrationActivity.this);
        //progressDialog.setMessage("Loading....");
        //progressDialog.setCancelable(false);
        //progressDialog.show();
        System.out.println("spinnerapi" + spinnerHubCall.request().url().toString());

        spinnerHubCall.enqueue(new Callback<List<SpinnerHub>>() {
            @Override
            public void onResponse(Call<List<SpinnerHub>> call, Response<List<SpinnerHub>> response) {
                //progressDialog.dismiss();
                System.out.println("spinnerresponse" + response.body());
                //  SpinnerHub spinnerHub=response.body();
                Const.list = response.body();

                System.out.println("listsize" + Const.list.size());

            }

            @Override
            public void onFailure(Call<List<SpinnerHub>> call, Throwable t) {
                System.out.println("reee" + t.getMessage());
            }
        });


    }

}
