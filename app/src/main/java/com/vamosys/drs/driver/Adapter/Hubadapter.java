package com.vamosys.drs.driver.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.vamosys.drs.driver.R;
import com.vamosys.drs.driver.Response.SpinnerHub;

import java.util.List;

public class Hubadapter  extends BaseAdapter {
    private final Activity context;
    private final List<SpinnerHub> maintitle;
    private String language;

    public Hubadapter(Activity context, List<SpinnerHub> maintitle, String language) {
        super();
        // TODO Auto-gener stub

        this.context=context;
        this.maintitle=maintitle;
        this.language = language;
    }

    @Override
    public int getCount() {
        return maintitle.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.my_list, null,true);

        TextView titleText = (TextView) rowView.findViewById(R.id.title);
        if (language.equalsIgnoreCase("english")){
            titleText.setText(maintitle.get(position).getCity());
        }else if (language.equalsIgnoreCase("hindi")){
            titleText.setText(maintitle.get(position).getHindi());
        }


        return rowView;

    };
}
