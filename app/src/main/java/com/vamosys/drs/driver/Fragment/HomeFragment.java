package com.vamosys.drs.driver.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.vamosys.drs.driver.Activity.AddCheckList1;
import com.vamosys.drs.driver.ApiClient.ApiClient;
import com.vamosys.drs.driver.ApiClient.ApiInterface;
import com.vamosys.drs.driver.R;
import com.vamosys.drs.driver.ApiClient.Const;
import com.vamosys.drs.driver.Response.NewTripResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment implements View.OnClickListener {
    String Tag="response driver";
    Button btn_accept,btn_reject;
    LinearLayout btn_createCheklist;
    RelativeLayout rel_all;
    TextView txt_no_record,txt_vehiclename,txt_PickupLoc,txt_ContactPerson,txt_dropLoc,txt_pickUpTime;
    ApiInterface apiInterface;
    List<NewTripResponse> newTripResponses=new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View homeLayout= inflater.inflate(R.layout.fragment_home, container, false);
        btn_accept=homeLayout.findViewById(R.id.btn_accept);
        btn_reject=homeLayout.findViewById(R.id.btn_reject);
        btn_createCheklist=homeLayout.findViewById(R.id.btn_create_checklist);
        rel_all=homeLayout.findViewById(R.id.lay_all);
        txt_no_record=homeLayout.findViewById(R.id.txt_norecord);
        btn_accept.setOnClickListener(this);
        btn_reject.setOnClickListener(this);
        btn_createCheklist.setOnClickListener(this);
        txt_vehiclename=homeLayout.findViewById(R.id.txt_vehiclename);
        txt_PickupLoc=homeLayout.findViewById(R.id.txt_pickuplocation);
        txt_ContactPerson=homeLayout.findViewById(R.id.txt_contactPerson);
        txt_dropLoc=homeLayout.findViewById(R.id.txt_drop_location);
        txt_pickUpTime=homeLayout.findViewById(R.id.txt_pickuptime);
        apiInterface=ApiClient.getClient().create(ApiInterface.class);
        Const.driverId="1";
        Const.VehicleId="PAMUSA01";
        getNewTripDetail();
        return homeLayout;
    }

    private void getNewTripDetail() {
        Call<List<NewTripResponse>> call=apiInterface.getNewTrip("1","PAMUSA01");
        Log.d(Tag, " data check "+call.request().url().toString());
        call.enqueue(new Callback<List<NewTripResponse>>() {
            @Override
            public void onResponse(Call<List<NewTripResponse>> call, Response<List<NewTripResponse>> response) {
                newTripResponses=response.body();
                Log.d(Tag,"response sucess "+newTripResponses.size());
                if(newTripResponses.size()>0){
                    NewTripResponse n =newTripResponses.get(0);
                    if(n.getVehicleName()!=null){
                        txt_vehiclename.setText(n.getVehicleName());
                    }
                    if(n.getPickupLocation()!=null){
                        txt_PickupLoc.setText(n.getPickupLocation());
                    }
                    if(n.getContactPerson()!=null){
                        txt_ContactPerson.setText(n.getContactPerson());
                    }
                    if(n.getNextDestination()!=null){
                        txt_dropLoc.setText(n.getNextDestination());
                    }
                    if(n.getPickupDateTime()==0){
                        txt_pickUpTime.setText(Const.convertMonthComa(n.getPickupDateTime()));
                    }
                }
                else{
                    Log.d(Tag,"response sucess "+newTripResponses.size());
                    rel_all.setVisibility(View.GONE);
                    txt_no_record.setVisibility(View.VISIBLE);
                }

            }
            @Override
            public void onFailure(Call<List<NewTripResponse>> call, Throwable t) {
                Log.d(Tag,"fail "+t.getMessage());
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_accept:
                takeActionForNewTrip(1,"109","accepted");
                break;
            case R.id.btn_reject:
                showAlertDialog("100");
                break;
            case R.id.btn_create_checklist:
                startActivity(new Intent(getActivity(), AddCheckList1.class));
                break;
        }
    }
    public void takeActionForNewTrip(int status,String SeqNum,String reason) {
        if (status != 0) {
            Call<String> call = apiInterface.driverTakeActionForNewTrip("1", "PAMUSA01",
                    status + "", SeqNum + "", reason);
            Log.d(Tag, " data check "+call.request().url().toString());
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    Log.d(Tag, " data " + response.body());
                    btn_accept.setVisibility(View.GONE);
                    btn_reject.setVisibility(View.GONE);
                    Log.d(Tag, " data " + status);
                    if(status==1) {
                        Log.d(Tag, " data " + status);
                        btn_createCheklist.setVisibility(View.VISIBLE);
                    }
                    else{
                        Log.d(Tag, " data " + status);
                        rel_all.setVisibility(View.GONE);
                        txt_no_record.setVisibility(View.VISIBLE);
                    }
                }
                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.d(Tag, " Fail" + t.getMessage());
                    Toast.makeText(getContext(), "Oops something went wrong...", Toast.LENGTH_SHORT).show();
                }
            });
        }
        else{
            Log.d(Tag, " Fail" + "status null");
        }
    }
    private void showAlertDialog(String SeqNum){
        Log.d("show alert","show3");

        AlertDialog.Builder builders = new AlertDialog.Builder(getContext(),R.style.CustomAlertDialog);
        final View customLayout = LayoutInflater.from(getContext()).inflate(R.layout.remarks_alert, null);
        EditText txt_Message=customLayout.findViewById(R.id.txt_message);
        Button btn_ok=customLayout.findViewById(R.id.btn_submit);
        builders.setView(customLayout);
        ImageView img_close=customLayout.findViewById(R.id.img_close);
        final AlertDialog dialogs = builders.create();
        dialogs.show();
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txt_Message.getText().toString().length()>1) {
                    takeActionForNewTrip(2, SeqNum, txt_Message.getText().toString());
                    dialogs.dismiss();
                }
                else {
                    Toast.makeText(getContext(), "Please Enter remarks", Toast.LENGTH_SHORT).show();
                }
            }
        });
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_accept.setVisibility(View.VISIBLE);
                btn_reject.setVisibility(View.VISIBLE);
                dialogs.dismiss();
            }
        });
    }
}
