package com.vamosys.drs.driver;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.vamosys.drs.driver.Activity.SigninActivity;
import com.vamosys.drs.driver.Fragment.HomeFragament2;
import com.vamosys.drs.driver.Fragment.HomeFragment;
import com.vamosys.drs.driver.Fragment.MailFragment;
import com.vamosys.drs.driver.Fragment.MapFragment;

public class HomeBottomNavigation extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener, NavigationView.OnNavigationItemSelectedListener {
    SharedPreferences preferences;
    CheckListPereference checkListPereference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        setContentView(R.layout.activity_home_bottom_navigation);
        checkListPereference=new CheckListPereference(HomeBottomNavigation.this);
        if(checkListPereference.getIsCheckListAdded()){
            loadFragment(new HomeFragament2());
        }else{
            loadFragment(new HomeFragment());
        }
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ImageView navback=findViewById(R.id.navigation_icon);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(false);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.openDrawer(GravityCompat.START);
            }
        });
        BottomNavigationView navView = findViewById(R.id.botm_nav_view);
        navView.setOnNavigationItemSelectedListener(this);
    }
    private Boolean loadFragment(Fragment fragment){
        if(fragment!=null){
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
           return true;
        }
        else{
           return false;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Boolean ret_val=false;
        Fragment fragment=null;
        switch (item.getItemId()){
            case R.id.navigation_home:
                if(checkListPereference.getIsCheckListAdded()){
                    fragment=new HomeFragament2();
                }else{
                    fragment=new HomeFragment();
                }
                ret_val=loadFragment(fragment);
                break;
            case R.id.navigation_mail:
                fragment=new MailFragment();
                ret_val=loadFragment(fragment);
                break;
            case R.id.navigation_map:
                fragment=new MapFragment();
                ret_val=loadFragment(fragment);
                break;
            case R.id.home:
                startActivity(new Intent(HomeBottomNavigation.this,HomeBottomNavigation.class));
                ret_val=true;
                break;
            case R.id.logut:
                Log.d("side_menu","logout");
                preferences = getSharedPreferences("login_session", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor=preferences.edit();
                editor.clear();
                editor.commit();
                startActivity(new Intent(HomeBottomNavigation.this, SigninActivity.class));
                finish();
                break;
            case R.id.setting:
                Log.d("side_menu","setting");
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        return ret_val;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            Intent homeScreenIntent = new Intent(Intent.ACTION_MAIN);
            homeScreenIntent.addCategory(Intent.CATEGORY_HOME);
            homeScreenIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(homeScreenIntent);
            finish();
        }
    }
}
