package com.vamosys.drs.driver.Activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.google.gson.Gson;
import com.vamosys.drs.driver.ApiClient.ApiClient;
import com.vamosys.drs.driver.ApiClient.ApiInterface;
import com.vamosys.drs.driver.AppPreference;
import com.vamosys.drs.driver.PermissionCheck;
import com.vamosys.drs.driver.R;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.loader.content.CursorLoader;

import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.vamosys.drs.driver.R.string.eaddress;
import static com.vamosys.drs.driver.R.string.edob;
import static com.vamosys.drs.driver.R.string.elicnum;
import static com.vamosys.drs.driver.R.string.eprevious;


public class LoginActivity extends LocalizationActivity {
    private LinearLayout lyt_btn;
    EditText et_dob, et_licence, et_address, et_addlicense, et_aadhar, et_previous, et_pan;
    final Calendar myCalendar = Calendar.getInstance();
    AppPreference appPreference;
    ApiInterface apiInterface;
    private static final int PICK_IMAGE = 100;
    int uploadImg = 0;
    Uri licenceuri, aadharuri;
    String imagePath, licencename;
    BottomSheetDialog mBottomSheetDialog;
    LinearLayout ln_camera, ln_gallery, ln_cancel;
    private static final int REQUEST_CAPTURE_IMAGE = 1;
    private static final int REQUEST_TAKE_GALLERY_PHOTO = 300;
    private Uri selectedImageUri;
    String base64Image, imageFilePath;
    String licencevalid, aadharvalid;
    String myjson;
    Gson gson;
    Bacward pojo = new Bacward();
    String  bno,bdob,badd,bprev;

    String nnam,npho;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getPermission();
        appPreference = new AppPreference(this);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        if (appPreference.getLangName().equalsIgnoreCase("english")) {
            setLanguage("en");
        }
        if (appPreference.getLangName().equalsIgnoreCase("hindi")) {
            setLanguage("hi");
        }


        System.out.println("licceee" + pojo.getLicno());
        findviewid();

        // get Intent Next
        nnam = getIntent().getStringExtra("NNAM");
        npho=getIntent().getStringExtra("NPHO");

        // getIntent back
        bno = getIntent().getStringExtra("BNO");
        bdob=getIntent().getStringExtra("BDOB");
        badd=getIntent().getStringExtra("BADD");
        bprev=getIntent().getStringExtra("BPREV");
        System.out.println("bsdfg"+bno);


            et_licence.setText(bno);
            et_dob.setText(bdob);
            et_address.setText(badd);
            et_previous.setText(bprev);


    }

    private void findviewid() {
        et_licence = findViewById(R.id.et_license);
        et_address = findViewById(R.id.et_address);
        et_addlicense = findViewById(R.id.et_add_licence);
        et_aadhar = findViewById(R.id.et_aadhar);
        et_previous = findViewById(R.id.et_previous);
        lyt_btn = findViewById(R.id.lyt_login);
        et_dob = findViewById(R.id.et_dob);
        et_pan = findViewById(R.id.et_pan);
        et_addlicense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadImg = 0;
                if (PermissionCheck.checkhaspermission(LoginActivity.this)) {

                    // openGallery(PICK_IMAGE);
                    mBottomSheetDialog.show();
                } else {
                    PermissionCheck.enablepermission(LoginActivity.this);
                    Log.d("selectedimg", "failure");
                }


            }
        });

        et_aadhar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadImg = 1;
                // openGallery(PICK_IMAGE);
                mBottomSheetDialog.show();
            }
        });

        mBottomSheetDialog = new BottomSheetDialog(LoginActivity.this);
        LayoutInflater li = (LayoutInflater) LoginActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View sheetView = li.inflate(R.layout.bottom_dialogbox_image, null);
        mBottomSheetDialog.setContentView(sheetView);
        ((View) sheetView.getParent()).setBackgroundColor(LoginActivity.this.getResources().getColor(android.R.color.transparent));
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) sheetView.getParent()).getLayoutParams();
        params.setMargins(50, 50, 50, 0); // set margin as your wish.
        ln_camera = (LinearLayout) sheetView.findViewById(R.id.ln_camera);
        ln_gallery = (LinearLayout) sheetView.findViewById(R.id.ln_gallery);
        ln_cancel = (LinearLayout) sheetView.findViewById(R.id.ln_cancel);


        ln_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_CAPTURE_IMAGE);
                }
                mBottomSheetDialog.dismiss();
            }
        });
        ln_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_PICK);
                // startActivityForResult(Intent.createChooser(intent, "Select Image"), REQUEST_TAKE_GALLERY_PHOTO);
                startActivityForResult(intent, REQUEST_TAKE_GALLERY_PHOTO);
                mBottomSheetDialog.dismiss();
            }
        });

        ln_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });

        lyt_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_licence.getText().toString().length() == 0) {
                    et_licence.setError(getText(elicnum));
                } else if (et_dob.getText().toString().length() == 0) {
                    et_dob.setError(getText(edob));
                } else if (et_address.getText().toString().length() == 0) {
                    et_address.setError(getText(eaddress));
                } else if (et_previous.getText().toString().length() == 0) {
                    et_previous.setError(getText(eprevious));
                }
                else if (et_addlicense.getText().toString().equalsIgnoreCase("")){
                    et_addlicense.setError(getResources().getString(R.string.Pleaseaddlicence));
                }
                else if (et_aadhar.getText().toString().equalsIgnoreCase("")){
                    et_aadhar.setError(getResources().getString(R.string.Pleaseaddlicence));
                }
                else {
                    gotoLogin();
                }
            }
        });

        et_dob.addTextChangedListener(new TextWatcher() {
            int prevL = 0;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                prevL = et_dob.getText().toString().length();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int length = editable.length();
                if ((prevL < length) && (length == 2 || length == 5)) {
                    editable.append("/");
                }
            }
        });
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

//        et_dob.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Calendar mcurrentDate = Calendar.getInstance();
//                final int year = mcurrentDate.get(Calendar.YEAR);
//                final int month = mcurrentDate.get(Calendar.MONTH);
//                final int day = mcurrentDate.get(Calendar.DAY_OF_MONTH);
//                System.out.println("dadd" + day);
//                Calendar twoDaysAgo = (Calendar) mcurrentDate.clone();
//                twoDaysAgo.add(Calendar.DATE, -2);
//                Calendar twoDaysLater = (Calendar) mcurrentDate.clone();
//                twoDaysLater.add(Calendar.DATE, 2);
//                //   datePicker.setMinDate(twoDaysAgo.getTimeInMillis());
//                // datePicker.setMaxDate(twoDaysLater.getTimeInMillis());
//                System.out.println("min" + (twoDaysAgo.getTimeInMillis()));
//                System.out.println("max" + (twoDaysAgo.getTimeInMillis()));
//                final DatePickerDialog mDatePicker = new DatePickerDialog(LoginActivity.this, new DatePickerDialog.OnDateSetListener() {
//                    @Override
//                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
//
//                        Calendar userAge = new GregorianCalendar(selectedyear, selectedmonth, selectedday);
//                        Calendar minAdultAge = new GregorianCalendar();
//                        minAdultAge.add(Calendar.YEAR, -18);
//                        if (minAdultAge.before(userAge)) {
//                           Toast.makeText(LoginActivity.this, R.string.below, Toast.LENGTH_SHORT).show();
//                        } else {
//                            et_dob.setText(new StringBuilder().append(selectedday).append("-").append(selectedmonth + 1).append("-").append(selectedyear));
//
//                        }
//
//
//                        int month_k = selectedmonth + 1;
//                        System.out.println("kjdhk" + month_k);
//
//                    }
//                }, year - 18, month, day - 1);
//                mDatePicker.setTitle("Please select date");
//                // TODO Hide Future Date Here
//                mDatePicker.getDatePicker().setMaxDate(myCalendar.getTimeInMillis());
//
//                // TODO Hide Past Date Here
//                //  mDatePicker.getDatePicker().setMinDate(System.currentTimeMillis());
//                mDatePicker.show();
//
//            }
//        });
    }

    private void updateLabel() {
        String myFormat = "dd/MM/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        et_dob.setText(sdf.format(myCalendar.getTime()));
    }

    public void gotoLogin() {
        System.out.println("addlic"+et_addlicense.getText().toString());
        String lnumber = et_licence.getText().toString();
        String dob = et_dob.getText().toString();
        String address = et_address.getText().toString();
        String previous = et_previous.getText().toString();
        System.out.println("myname" + appPreference.getAccName());
        System.out.println("phone" + appPreference.getPhone());
        System.out.println("hub" + appPreference.getHub());
        appPreference.setLicense(et_licence.getText().toString());
        appPreference.setDob(et_dob.getText().toString());
        appPreference.setAddress(et_address.getText().toString());
        appPreference.setPrevious_Company(et_previous.getText().toString());
        System.out.println("hsegfe" + licencevalid);
        System.out.println("grge" + aadharvalid);

        appPreference.setLicenseNUMBER(licencevalid);
        appPreference.setAadharnumber(aadharvalid);
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
    }

   /* private void openGallery(int pickImage) {

        Intent galleryintent = new Intent(Intent.ACTION_PICK);
        galleryintent.setType("image/*");

        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        Intent chooser = new Intent(Intent.ACTION_CHOOSER);
        chooser.putExtra(Intent.EXTRA_INTENT, galleryintent);
        chooser.putExtra(Intent.EXTRA_TITLE, "Select from:");

        Intent[] intentArray = {cameraIntent};
        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
        startActivityForResult(chooser, PICK_IMAGE);
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

      /*  if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            switch (uploadImg) {
                case 0:
                    licenceuri = data.getData();
                    licencename = "Licence";
                    imagePath = getRealPathFromUri(licenceuri);
                    Log.d("response", imagePath);
                    uploadImageServer();
                    //uploadFile(licenceuri);

                    break;
                case 1:
                    aadharuri = data.getData();
                    System.out.println("uriii" + aadharuri);+
                    uploadImageServer();
                    //  uploadFile(aadharuri);
                    break;

            }

        }*/

        switch (uploadImg) {

            case 0:
                File file;

                System.out.println("here");
                if (resultCode == RESULT_OK && requestCode == REQUEST_TAKE_GALLERY_PHOTO && data != null) {

                    licenceuri = data.getData();
                    System.out.println("galll" + licenceuri);

                    String[] projection = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getContentResolver().query(licenceuri, projection, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(projection[0]);
                    imageFilePath = cursor.getString(columnIndex); // returns null
                    cursor.close();

                    licencename = "Licence";
                    imagePath = getRealPathFromUri(licenceuri);

                    System.out.println("sh" + imagePath);

                    //   et_addlicense.setText(imagePath);
                    Log.d("response", imagePath);
                    uploadImageServer();

                    file = new File(imagePath);
                    et_addlicense.setText("" + file.getName());
                    licencevalid = file.getName();
                } else if (resultCode == RESULT_OK && requestCode == REQUEST_CAPTURE_IMAGE && data != null) {


                    Bundle extra = data.getExtras();
                    Bitmap bitmap = (Bitmap) extra.get("data");
                    licenceuri = getImageUri(getApplicationContext(), bitmap);
                    System.out.println("cammm" + licenceuri);

                    imagePath = getRealPathFromUri(licenceuri);

                    //  et_addlicense.setText(imagePath);
                    uploadImageServer();

                    file = new File(imagePath);
                    et_addlicense.setText("" + file.getName());
                    licencevalid = file.getName();
                }

                break;


            case 1:
                File file1;
                System.out.println("here");
                if (resultCode == RESULT_OK && requestCode == REQUEST_TAKE_GALLERY_PHOTO && data != null) {

                    licenceuri = data.getData();
                    System.out.println("galll" + licenceuri);
                    final InputStream imageStream;

                    try {
                        imageStream = getContentResolver().openInputStream(licenceuri);
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                        licencename = "Licence";
                        imagePath = getRealPathFromUri(licenceuri);

                        // et_aadhar.setText(imagePath);
                        Log.d("response", imagePath);
                        uploadImageServer();
                        file1 = new File(imagePath);
                        et_aadhar.setText("" + file1.getName());
                        aadharvalid = file1.getName();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }


                } else if (resultCode == RESULT_OK && requestCode == REQUEST_CAPTURE_IMAGE && data != null) {


                    Bundle extra = data.getExtras();
                    Bitmap bitmap = (Bitmap) extra.get("data");
                    licenceuri = getImageUri(getApplicationContext(), bitmap);
                    System.out.println("cammm" + licenceuri);

                    imagePath = getRealPathFromUri(licenceuri);

                    //  et_aadhar.setText(imagePath);
                    uploadImageServer();
                    file1 = new File(imagePath);
                    et_aadhar.setText("" + file1.getName());
                    aadharvalid = file1.getName();

                }


                break;
        }


    }






/*
    private void uploadCamImageServer() {
        System.out.println("imagePath" + imagePath);
        File file = new File(imagePath);
        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);

        System.out.println("requestBody" + requestBody);
        MultipartBody.Part body = MultipartBody.Part.createFormData("uploadFiles", file.getName(), requestBody);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.uploadimage(body);
        System.out.println("responseurl" + call.request().url().toString());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String myresult = "" + response.body().string();
                    System.out.println("responselogin1" + myresult);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Toast.makeText(LoginActivity.this, "Success", Toast.LENGTH_LONG).show();

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "fail", Toast.LENGTH_LONG).show();
                System.out.println("responselogin2" + t.getMessage());

            }
        });


    }*/


    private void uploadImageServer() {
        System.out.println("imagePath" + imagePath);
        File file = new File(imagePath);
        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);

        System.out.println("requestBody" + requestBody);
        MultipartBody.Part body = MultipartBody.Part.createFormData("uploadFiles", file.getName(), requestBody);
        System.out.println("jhsdgsa" + body);

        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.uploadimage(body);
        System.out.println("responseurl" + call.request().url().toString());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String myresult = "" + response.body().string();
                    System.out.println("responselogin1" + myresult);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.println("responselogin2" + t.getMessage());

            }
        });


    }


    private void uploadImageServer2() {
        System.out.println("imagePath" + imagePath);
        File file = new File(imagePath);

        try {
            File compressedImageFile = new Compressor(getApplicationContext()).compressToFile(file);
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), compressedImageFile);
            MultipartBody.Part imagenPerfil = MultipartBody.Part.createFormData("UploadFile" + ".jpg", compressedImageFile.getName(), requestFile);
            // RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);

            //   System.out.println("requestBody222" + requestBody);

            // MultipartBody.Part body = MultipartBody.Part.createFormData("uploadFiles", file.getName(), requestBody);
            //System.out.println("regreefg"+body);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<ResponseBody> call = apiInterface.uploadimage(imagenPerfil);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        String myresult = "" + response.body().string();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getContentResolver() != null) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }


    private String getRealPathFromUri(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(getApplicationContext(), uri, projection, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_idx = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_idx);
        cursor.close();
        return result;
    }

    private void getPermission() {
        String[] params = null;
        String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        String readExternalStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
        String cameraStorage = Manifest.permission.CAMERA;

        int hasWriteExternalStoragePermission = ActivityCompat.checkSelfPermission(getApplicationContext(), writeExternalStorage);
        int hasReadExternalStoragePermission = ActivityCompat.checkSelfPermission(getApplicationContext(), readExternalStorage);

        int hasCameraStoragePermission = ActivityCompat.checkSelfPermission(getApplicationContext(), cameraStorage);

        List<String> permissions = new ArrayList<String>();

        if (hasWriteExternalStoragePermission != PackageManager.PERMISSION_GRANTED)
            permissions.add(writeExternalStorage);
        if (hasReadExternalStoragePermission != PackageManager.PERMISSION_GRANTED)
            permissions.add(readExternalStorage);


        if (hasCameraStoragePermission != PackageManager.PERMISSION_GRANTED)
            permissions.add(cameraStorage);


        if (!permissions.isEmpty()) {
            params = permissions.toArray(new String[permissions.size()]);
        }

        if (params != null && params.length > 0) {
            ActivityCompat.requestPermissions(LoginActivity.this,
                    params,
                    100);
        }
    }

    @Override
    public void onBackPressed() {
     /*   appPreference.setlNo(et_licence.getText().toString());
        appPreference.setlDob(et_dob.getText().toString());
        appPreference.setlAddress(et_address.getText().toString());
        appPreference.setlPrevious(et_previous.getText().toString());

        finish();*/
     Intent i=new Intent(getApplicationContext(),RegistrationActivity.class);
     i.putExtra("NO",et_licence.getText().toString());
     i.putExtra("DOB",et_dob.getText().toString());
        i.putExtra("ADDRESS",et_address.getText().toString());
        i.putExtra("PREV",et_previous.getText().toString());
        i.putExtra("BNAM",nnam);
        i.putExtra("BPHO",npho);
     startActivity(i);
    }

}
