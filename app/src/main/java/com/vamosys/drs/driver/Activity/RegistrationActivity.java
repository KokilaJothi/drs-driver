package com.vamosys.drs.driver.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.vamosys.drs.driver.Adapter.Hubadapter;
import com.vamosys.drs.driver.ApiClient.ApiClient;
import com.vamosys.drs.driver.ApiClient.ApiInterface;
import com.vamosys.drs.driver.ApiClient.Const;
import com.vamosys.drs.driver.AppPreference;
import com.vamosys.drs.driver.R;
import com.vamosys.drs.driver.Response.SpinnerHub;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends LocalizationActivity {
    EditText et_name, et_phone;
    private LinearLayout next;
    EditText sp_hub;
    ArrayList<String> arr_hub = new ArrayList<>();
    ApiInterface apiInterface;
    AppPreference appPreference;
    String hub, mobilenumber;
    TextView txt_eng, txt_hindi;
    String no, dob, add, prev;
    String bnam,bpho;
    List<SpinnerHub> list;
    String hubname;
    ListView listView;
    Dialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        mobilenumber = getIntent().getStringExtra("mobilenumber");
        appPreference = new AppPreference(this);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        System.out.println("num" + appPreference.getlNo());
        System.out.println("dobbb" + appPreference.getlDob());
        System.out.println("addd" + appPreference.getlAddress());
        System.out.println("previ" + appPreference.getlPrevious());
        if (appPreference.getLangName().equalsIgnoreCase("english")) {
            setLanguage("en");
        }
        if (appPreference.getLangName().equalsIgnoreCase("hindi")) {
            setLanguage("hi");
        }


        findviewid();
        getHub();
        // Login to Reg
        bnam = getIntent().getStringExtra("BNAM");
        bpho=getIntent().getStringExtra("BPHO");
        et_name.setText(bnam);
        if (bpho!=null) {
            et_phone.setText(bpho);
            et_phone.setEnabled(false);
        }
        no = getIntent().getStringExtra("NO");
        dob = getIntent().getStringExtra("DOB");
        add = getIntent().getStringExtra("ADDRESS");
        prev = getIntent().getStringExtra("PREV");
        System.out.println("ff" + no);
    }

    private void findviewid() {

        next = findViewById(R.id.Next_btn);
        sp_hub = findViewById(R.id.edt_state);
        et_name = findViewById(R.id.et_name);
        et_phone = findViewById(R.id.et_phone);
        et_phone.setEnabled(false);
        et_phone.setText(mobilenumber);

        txt_eng = findViewById(R.id.txt_eng);
        txt_hindi = findViewById(R.id.txt_hindi);
        txt_eng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLanguage("en");
                appPreference.setLangName("english");

            }
        });
        txt_hindi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLanguage("hi");
                appPreference.setLangName("hindi");

            }
        });
       sp_hub.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               System.out.println("sphubclick   ");
               dialog = new Dialog(RegistrationActivity.this);

                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
               dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
               dialog.setContentView(R.layout.dialog_city);
               WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
               lp.copyFrom(dialog.getWindow().getAttributes());
               lp.width = WindowManager.LayoutParams.MATCH_PARENT;
               lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
               lp.gravity = Gravity.CENTER;

               dialog.getWindow().setAttributes(lp);
               listView=dialog.findViewById(R.id.listview);
//               getHub();
//               Hubadapter adapter=new Hubadapter(RegistrationActivity.this, maintitle, subtitle);
//
//               listView.setAdapter(adapter);
               if (appPreference.getLangName().equalsIgnoreCase("english")) {
                   Hubadapter adapter=new Hubadapter(RegistrationActivity.this,Const.list,"english");
                   listView.setAdapter(adapter);

               }
               if (appPreference.getLangName().equalsIgnoreCase("hindi")) {
                   Hubadapter adapter=new Hubadapter(RegistrationActivity.this,Const.list,"hindi");
                   listView.setAdapter(adapter);
               }



               listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                   @Override
                   public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                       if (appPreference.getLangName().equalsIgnoreCase("english")) {
                           sp_hub.setText(Const.list.get(position).getCity());
                       }else if (appPreference.getLangName().equalsIgnoreCase("hindi")) {
                           sp_hub.setText(Const.list.get(position).getHindi());
                       }

                       hubname = Const.list.get(position).getHubName();
                       Log.d("hubname  ",""+Const.list.get(position).getHubName());
                       dialog.dismiss();

                   }
               });

               dialog.show();
           }
       });
//        sp_hub.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                System.out.println("");
//
//
//                hubname = list.get(position).getHubName();
//                Toast.makeText(RegistrationActivity.this, hubname, Toast.LENGTH_SHORT).show();
//
//
//
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (et_name.getText().toString().length() == 0) {
                    et_name.setError(getText(R.string.ename));
                } else if (et_phone.getText().toString().length() == 0) {
                    et_phone.setError(getText(R.string.ephone));
                } else if (et_phone.getText().toString().length() < 10) {
                    et_phone.setError(getText(R.string.evalidpho));
                } else if (sp_hub.getText().toString().length() == 0) {
                    sp_hub.setError(getText(R.string.hub));
                } else {
                    appPreference.setAccName(et_name.getText().toString());
                    appPreference.setPhone(et_phone.getText().toString());
                    System.out.println("selectedhub   "+hubname);
                    appPreference.setHub(hubname);
                    Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                    System.out.println("sfsds" + no);
                    i.putExtra("BNO", no);
                    i.putExtra("BDOB", dob);
                    i.putExtra("BADD", add);
                    i.putExtra("BPREV", prev);
                    i.putExtra("NNAM", et_name.getText().toString());
                    i.putExtra("NPHO", et_phone.getText().toString());
                    startActivity(i);
                    //startActivity(new Intent(RegistrationActivity.this, LoginActivity.class));
                }

            }
        });

    }


    private void getHub() {

        final Call<List<SpinnerHub>> spinnerHubCall = apiInterface.getSpinnerHub();
        //final ProgressDialog progressDialog;
        //progressDialog = new ProgressDialog(RegistrationActivity.this);
        //progressDialog.setMessage("Loading....");
        //progressDialog.setCancelable(false);
        //progressDialog.show();
        System.out.println("spinnerapi" + spinnerHubCall.request().url().toString());

        spinnerHubCall.enqueue(new Callback<List<SpinnerHub>>() {
            @Override
            public void onResponse(Call<List<SpinnerHub>> call, Response<List<SpinnerHub>> response) {
                //progressDialog.dismiss();
                System.out.println("spinnerresponse" + response.body());
                //  SpinnerHub spinnerHub=response.body();
                list = response.body();

                System.out.println("listsize" + list.size());

//
//                for (int i = 0; i < list.size(); i++) {
//                    if (list.get(i).getCity().equals("")){
//                        arr_hub.add(list.get(i).getState());
//                    }else {
//                        arr_hub.add(list.get(i).getCity());
//
//                    }
//
//                }

//                Collections.sort(arr_hub, new Comparator<String>() {
//                    @Override
//                    public int compare(String s1, String s2) {
//                        return s1.compareTo(s2);
//                    }
//                });



//                ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, arr_hub);
//                arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
               // sp_hub.setAdapter(arrayAdapter);
            }

            @Override
            public void onFailure(Call<List<SpinnerHub>> call, Throwable t) {
                System.out.println("reee" + t.getMessage());
            }
        });


    }


}
