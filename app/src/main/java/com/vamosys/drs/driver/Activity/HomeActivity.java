package com.vamosys.drs.driver.Activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.vamosys.drs.driver.AppPreference;
import com.vamosys.drs.driver.R;

public class HomeActivity extends LocalizationActivity {
    private Boolean exit = false;
    AppPreference appPreference;
    private int k = 0;

private Button customer_button;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        customer_button = findViewById(R.id.call_button);

        appPreference=new AppPreference(this);
        if (appPreference.getLangName().equalsIgnoreCase("english")) {
            setLanguage("en");
        }
        if (appPreference.getLangName().equalsIgnoreCase("hindi")) {
            setLanguage("hi");
        }

        customer_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:+917397222622"));
                //intent.setData(Uri.parse("+917397222622"));
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        k = 0;
    }

    @Override
    public void onBackPressed() {
        k++;
        if(k == 1) {
            Toast.makeText(HomeActivity.this,"Double Click to exist from app", Toast.LENGTH_SHORT).show();
        } else {
            //exit app to home screen
            Intent homeScreenIntent = new Intent(Intent.ACTION_MAIN);
            homeScreenIntent.addCategory(Intent.CATEGORY_HOME);
            homeScreenIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(homeScreenIntent);
        }
    }
}