package com.vamosys.drs.driver.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.vamosys.drs.driver.ApiClient.ApiClient;
import com.vamosys.drs.driver.ApiClient.ApiInterface;
import com.vamosys.drs.driver.ApiClient.Const;
import com.vamosys.drs.driver.AppPreference;
import com.vamosys.drs.driver.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtpActivity extends LocalizationActivity implements TextWatcher {
    EditText one, two, three, four, five, six;
    LinearLayout btn_single;
    EditText otp1, otp2, otp3, otp4, otp5, otp6;
    String mobileotp, verificationid, mobilenumber;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    // private OtpEditText otpcode;
    FirebaseFirestore db;
    private String isexist;
    private String phonenumber;
    Boolean userExist = false;
   AppPreference appPreference;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
//        mobileotp = getIntent().getStringExtra("mobilecode");
//        verificationid = getIntent().getStringExtra("verificationid");
        appPreference=new AppPreference(this);
        mobilenumber = getIntent().getStringExtra("mobilenumber");
        phonenumber = "+" + getIntent().getStringExtra("phonenumber");
        isexist = getIntent().getStringExtra("isexits");
        one = findViewById(R.id.editTextone);
        two = findViewById(R.id.editTexttwo);
        three = findViewById(R.id.editTextthree);
        four = findViewById(R.id.editTextfour);
        five = findViewById(R.id.editTextfive);
        six = findViewById(R.id.editTextsix);
        if (appPreference.getLangName().equalsIgnoreCase("english")) {
            setLanguage("en");
        }
        if (appPreference.getLangName().equalsIgnoreCase("hindi")) {
            setLanguage("hi");
        }

        one.addTextChangedListener(this);
        two.addTextChangedListener(this);
        three.addTextChangedListener(this);
        four.addTextChangedListener(this);
        five.addTextChangedListener(this);
        six.addTextChangedListener(this);
        System.out.println("otpisexist" + isexist);
        init();
        sendVerificationCode(phonenumber);
        firebaseAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        firebaseUser = firebaseAuth.getCurrentUser();

        btn_single = findViewById(R.id.btn_single);
        six.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                Log.d("otpVerify","otp entered");
                String o=one.getText().toString();
                String tw=two.getText().toString();
                String thr=three.getText().toString();
                String fou=four.getText().toString();
                String fiv=five.getText().toString();
                String si=six.getText().toString();
//                String optptxt1 = o+tw+thr+fou+fiv+six;
//                Toast.makeText(OtpActivity.this,"otp_btn"+optptxt1,Toast.LENGTH_LONG).show();
                if (o.length()==0 && tw.length()==0 && thr.length()==0 && fou.length()==0 && fiv.length()==0 && si.length()==0){
                    Log.d("otpVerify","null otp");
                    Toast.makeText(OtpActivity.this,getResources().getString(R.string.Enterotp), Toast.LENGTH_SHORT).show();
                }
                else {

                    String o1=one.getText().toString();
                    String tw1=two.getText().toString();
                    String thr1=three.getText().toString();
                    String fou1=four.getText().toString();
                    String fiv1=five.getText().toString();
                    String six1=six.getText().toString();
                    String optptxt = o1+tw1+thr1+fou1+fiv1+six1;
                    Log.d("otpVerify","entered otp"+optptxt);
                    verifyCode(optptxt);
                    postManualOtp("OTP Manually entered");
                }

                return false;
            }
        });
        btn_single.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String o=one.getText().toString();
                String tw=two.getText().toString();
                String thr=three.getText().toString();
                String fou=four.getText().toString();
                String fiv=five.getText().toString();
                String si=six.getText().toString();
                String optptxt1 = o+tw+thr+fou+fiv+six;
//                Toast.makeText(OtpActivity.this,"otp_btn"+optptxt1,Toast.LENGTH_LONG).show();
                if (o.length()==0 && tw.length()==0 && thr.length()==0 && fou.length()==0 && fiv.length()==0 && si.length()==0){
                   Toast.makeText(OtpActivity.this,getResources().getString(R.string.Enterotp), Toast.LENGTH_SHORT).show();


                }
                   else {
                    String o1=one.getText().toString();
                    String tw1=two.getText().toString();
                    String thr1=three.getText().toString();
                    String fou1=four.getText().toString();
                    String fiv1=five.getText().toString();
                    String six1=six.getText().toString();

                      String optptxt = o1+tw1+thr1+fou1+fiv1+six1;
                    verifyCode(optptxt);
                    postManualOtp("OTP Manually entered");

                }

            }
        });

    }

    private void sendVerificationCode(final String phonenumber) {
        Log.d("getsms", "phoneNumber " + phonenumber);
        Log.d("otpVerify","sendverfication");
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phonenumber,
                60,
                TimeUnit.SECONDS,
                OtpActivity.this,
                new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                    @Override
                    public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                        Log.d("getsms", "code " + phoneAuthCredential.getSmsCode());
                        Log.d("otpVerify","sendverfication"+" onverificationComplete "+phoneAuthCredential.getSmsCode());
                        mobileotp = phoneAuthCredential.getSmsCode();
                        if (mobileotp != null) {
//                            Toast.makeText(OtpActivity.this, "mobileOtp", Toast.LENGTH_SHORT).show();
                            Log.d("otpVerify","not null");
                            char[] chars = mobileotp.toCharArray();
                            System.out.println("charrrsss" + chars);
                            String c1 = String.valueOf(mobileotp.charAt(0));
                            String c2 = String.valueOf(mobileotp.charAt(1));
                            String c3 = String.valueOf(mobileotp.charAt(2));
                            String c4 = String.valueOf(mobileotp.charAt(3));
                            String c5 = String.valueOf(mobileotp.charAt(4));
                            String c6 = String.valueOf(mobileotp.charAt(5));

                            one.setText(c1);
                            two.setText(c2);
                            three.setText(c3);
                            four.setText(c4);
                            five.setText(c5);
                            six.setText(c6);
                           /* otpcode.setOnCompleteListener(new com.broooapps.otpedittext2.OnCompleteListener() {
                                @Override
                                public void onComplete(String value) {
                                    otpcode.setText(value);
                                }
                            });*/
                            Log.d("getsms", "call verifyCode");
                            verifyCode(mobileotp);
                            //otpcode.setText(mobileotp);
                        }
                        else {
//                            Toast.makeText(OtpActivity.this, "otp null", Toast.LENGTH_SHORT).show();
                            signInWithCredential(phoneAuthCredential);
                            Log.d("otpVerify","otp is null");
                            Log.d("getsms ","otp is null");

                            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
                            Call<ResponseBody> call = apiInterface.getstoreexception("","OTP NOT RECEIVED","GOOGLE PLAY SERVICES AUTOMATICALLY DETECT THE OTP",phonenumber);
                            Log.d("storeapiurl  ",""+call.request().url().toString());
                            call.enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    System.out.println("storeresponce   "+"Sucess");

                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    System.out.println("storeresponce   "+t.getMessage());
                                }
                            });
                        }

                    }

                    @Override
                    public void onVerificationFailed(@NonNull FirebaseException e) {

                        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
                        Call<ResponseBody> call = apiInterface.getstoreexception("","MOBILE NUMBEER IS WRONG","FirebaseAuthInvalidCredentialsException",phonenumber);
                        Log.d("storeapiurl  ",""+call.request().url().toString());
                        call.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                System.out.println("storeresponce   "+"Sucess");

                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                System.out.println("storeresponce   "+t.getMessage());
                            }
                        });
                        Log.d("codefail ", " " + e);
                        if (e instanceof FirebaseAuthInvalidCredentialsException) {
                            Log.d("codefail1 ", "Invalid request" + e);
                            // Invalid request
                            // ...
                        } else if (e instanceof FirebaseTooManyRequestsException) {
                            Log.d("code ail2 ", "sms exceed" + e);
                            // The SMS quota for the project has been exceeded
                            // ...
                        }

                    }

                    @Override
                    public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                        super.onCodeSent(s, forceResendingToken);
                        verificationid = s;
//                        Toast.makeText(OtpActivity.this,"verification id "+verificationid,Toast.LENGTH_LONG).show();
                        Log.d("getsms", "verify code " + s);
                        Log.d("otpVerify","on code sent");
                    }
                }
        );
    }

    private void verifyCode(String code) {
        Log.d("getsms", "verifycode" + code + " " + verificationid);
        System.out.println("nnnncc" + code);
        System.out.println("jhfd" + verificationid);
        try {
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationid, code);
            signInWithCredential(credential);
        } catch (Exception e) {
            Log.d("verifymessage  ",""+e.getMessage());
            Toast.makeText(this, "Verification Code is Wrong", Toast.LENGTH_SHORT).show();

            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<ResponseBody> call = apiInterface.getstoreexception("","Verification Code is Wrong","Verification Code is Wrong",phonenumber);
            Log.d("storeapiurl  ",""+call.request().url().toString());
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    System.out.println("storeresponce   "+"Sucess");
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    System.out.println("storeresponce   "+t.getMessage());
                }
            });
        }

    }

    private void signInWithCredential(PhoneAuthCredential credential) {
        Log.d("getsms", "sign credential");
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d("getsms", "completed0");
                        System.out.println("otpmobilenumber  " + mobilenumber);
                        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        appPreference.setUuid(user.getUid());
                        Log.d("firebaseUUIdd ",""+user.getUid());
                        Const.UUID = user.getUid();
                        Log.d("UUIdd  ", "uid no" + Const.UUID);
                        if (user.getUid() == null) {
                            Log.d("currentuid  ", "failure");
                            Log.d("getsms", "uid no null");
                        } else if (user.getUid() != null) {
                            Log.d("getsms", "uid no" + Const.UUID);
                            Log.d("getsms", "userexists" + Const.UserExists);
                            System.out.println("homeexist" + Const.isexit);
                            callIntent();

                        }


                    }

                });
    }

    private void savenumber(String mobilenumber) {
        Map<String, Object> newnumber = new HashMap<>();
        newnumber.put("mobilenumber", mobilenumber);
        CollectionReference user = db.collection("Users");

        user.add(newnumber).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                Toast.makeText(OtpActivity.this, "User added", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(OtpActivity.this, "User not added", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean validate() {
        if (otp1.getText().toString() != null) {
            Boolean checks = otp1.getText().length() > 0 ? true : false;
            if (otp2.getText().toString() != null) {
                checks = otp2.getText().length() > 0 ? true : false;
            }
            if (otp3.getText().toString() != null) {
                checks = otp3.getText().length() > 0 ? true : false;
            }
            if (otp4.getText().toString() != null) {
                checks = otp4.getText().length() > 0 ? true : false;
            }
            if (otp5.getText().toString() != null) {
                checks = otp5.getText().length() > 0 ? true : false;
            }
            if (otp6.getText().toString() != null) {
                checks = otp6.getText().length() > 0 ? true : false;
            }
            return checks;
        } else {
            return false;
        }

    }

    private void init() {
       // otpcode = findViewById(R.id.editTextCode);

    }

    private void callIntent() {
        if (Const.UserExists) {
            Log.d("getsms  ", "userexists");
            Intent intent = new Intent(OtpActivity.this, HomeActivity.class);
            //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
           // otpcode.setText("");
        } else {
            Const.Mobile_number = mobilenumber;
            Log.d("getsms", Const.Mobile_number);
            Log.d("getsms  ", "new user");
            Intent intent = new Intent(OtpActivity.this, RegistrationActivity.class);
            intent.putExtra("mobilenumber   ", mobilenumber);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

        if (s.length() == 1) {
            if (one.length() == 1) {
                two.requestFocus();
            }

            if (two.length() == 1) {
                three.requestFocus();
            }
            if (three.length() == 1) {
                four.requestFocus();
            }
            if (four.length()==1){
                five.requestFocus();
            }
            if (five.length()==1){
                six.requestFocus();
            }
        } else if (s.length() == 0) {
            if (six.length()==0){
                five.requestFocus();
            }
            if (five.length()==0){
                four.requestFocus();
            }
            if (four.length() == 0) {
                three.requestFocus();
            }
            if (three.length() == 0) {
                two.requestFocus();
            }
            if (two.length() == 0) {
                one.requestFocus();
            }
        }

    }
    private void postManualOtp(String message){
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.getstoreexception(Const.UUID,message,"Recived otp but automatic fetch is not happen",phonenumber);
        Log.d("storeapiurl  ",""+call.request().url().toString());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                System.out.println("storeresponce   "+"Sucess");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.println("storeresponce   "+t.getMessage());
            }
        });
    }
}
