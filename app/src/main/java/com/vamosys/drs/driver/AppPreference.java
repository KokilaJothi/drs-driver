package com.vamosys.drs.driver;

import android.content.Context;
import android.content.SharedPreferences;

public class AppPreference {

    private SharedPreferences mSharedPreferences;

    private static final String ACC_NAME = "Names";

    private static final String Phone = "Phone";
    private static final String UUID = "UUID";

    private static final String Hub = "Hub";

    private static final String License = "license";

    private static final String LicenseNUMBER = "licensenumber";
    private static final String AADHARNUMBER = "aadharnumber";

    private static final String DOB = "dob";

    private static final String Address = "address";

    private static final String Previous_Company = "previous";
    private static final String LANG_NAME = "Langs";

    private static final String IMG_CHECK = "imgcheck";


    private static final String L_NO = "lnumber";
    private static final String L_DOB = "ldob";
    private static final String L_ADDRESS = "laddress";
    private static final String L_PREVIOUS = "lprevious";
    private static final String L_PAN = "lpan";


    public AppPreference(Context context) {
        mSharedPreferences = context.getSharedPreferences("driver_registration", Context.MODE_PRIVATE);
    }

    public void setAccName(String name) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(ACC_NAME, name);
        System.out.println("acc"+name);
        edit.commit();
    }

    public String getAccName() {
        return mSharedPreferences.getString(ACC_NAME, "");
    }

    public void setPhone(String phone) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(Phone, phone);
        edit.commit();
    }

    public void setUuid(String uuid) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(UUID, uuid);
        edit.commit();
    }
    public String getUuid() {
        return mSharedPreferences.getString(UUID, "");
    }


    public String getPhone() {
        return mSharedPreferences.getString(Phone, "");
    }


    public void setHub(String hub) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(Hub, hub);
        edit.commit();
    }

    public String getHub() {
        return mSharedPreferences.getString(Hub, "");
    }


    public void setLicense(String license) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(License, license);
        edit.commit();
    }

    public String getLicense() {
        return mSharedPreferences.getString(License, "");
    }

    public void setDob(String dob) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(DOB, dob);
        edit.commit();
    }

    public String getDob() {
        return mSharedPreferences.getString(DOB, "");
    }

    public void setAddress(String address) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(Address, address);
        edit.commit();
    }

    public String getLangName() {
        return mSharedPreferences.getString(LANG_NAME, "");
    }

    public void setLangName(String orgId) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(LANG_NAME, orgId);
        edit.commit();
    }

    public String getAddress() {
        return mSharedPreferences.getString(Address, "");
    }

    public void setPrevious_Company(String previous_Company) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(Previous_Company, previous_Company);
        edit.commit();
    }

    public String getPrevious_Company() {
        return mSharedPreferences.getString(Previous_Company, "");
    }


    public void setlNo(String lNo) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(L_NO, lNo);
        System.out.println("acc"+lNo);
        edit.commit();
    }

    public String getlNo() {
        return mSharedPreferences.getString(L_NO, "");
    }



    public void setlDob(String lDob) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(L_DOB, lDob);
        System.out.println("acc"+lDob);
        edit.commit();
    }

    public String getlDob() {
        return mSharedPreferences.getString(L_DOB, "");
    }


    public void setlAddress(String lAddress) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(L_ADDRESS, lAddress);
        System.out.println("acc"+lAddress);
        edit.commit();
    }

    public String getlAddress() {
        return mSharedPreferences.getString(L_ADDRESS, "");
    }

    public void setlPrevious(String lPrevious) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(L_PREVIOUS, lPrevious);
        System.out.println("mmmm"+lPrevious);
        edit.commit();
    }

    public String getlPrevious() {
        return mSharedPreferences.getString(L_PREVIOUS, "");
    }

    public void setlPan(String name) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(L_PAN, name);
        System.out.println("acc"+name);
        edit.commit();
    }

    public String getlPan() {
        return mSharedPreferences.getString(L_PAN, "");
    }

    public void setImgCheck(String imgCheck) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(IMG_CHECK, imgCheck);
        System.out.println("acc"+imgCheck);
        edit.commit();
    }

    public String getImgCheck() {
        return mSharedPreferences.getString(IMG_CHECK, "");
    }


    public void setLicenseNUMBER(String licenseNUMBER) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(LicenseNUMBER, licenseNUMBER);
        System.out.println("acc"+licenseNUMBER);
        edit.commit();
    }

    public String getLicenseNUMBER() {
        return mSharedPreferences.getString(LicenseNUMBER, "");
    }

    public void setAadharnumber(String aadharnumber) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(AADHARNUMBER, aadharnumber);
        System.out.println("acc"+aadharnumber);
        edit.commit();
    }

    public String getAadharnumber() {
        return mSharedPreferences.getString(AADHARNUMBER, "");
    }

}
