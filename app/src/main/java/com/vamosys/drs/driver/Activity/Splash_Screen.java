package com.vamosys.drs.driver.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallState;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.OnSuccessListener;
import com.google.android.play.core.tasks.Task;
import com.vamosys.drs.driver.Choose_Language;
import com.vamosys.drs.driver.HomeBottomNavigation;
import com.vamosys.drs.driver.R;

import io.reactivex.internal.util.ErrorMode;

import static com.google.android.play.core.install.model.AppUpdateType.FLEXIBLE;
import static io.reactivex.internal.util.ErrorMode.IMMEDIATE;

public class Splash_Screen extends AppCompatActivity {
    private SharedPreferences preferences;
    private boolean loggin;
    AppUpdateManager appUpdateManager;
    private int MY_REQUEST_CODE = 1001;
//    InstallStateUpdatedListener listener;
//    Task<AppUpdateInfo> appUpdateInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash__screen);
//        startActivity(new Intent(Splash_Screen.this, HomeBottomNavigation.class));
//                    finish();

        preferences = getSharedPreferences("login_session", Context.MODE_PRIVATE);
//
        loggin = preferences.getBoolean("is_loggin",false);
//        Log.d("logginsession",""+loggin);
      Handler  handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

              if (!loggin){
                    Log.d("logginse","loninsuccess");
                    startActivity(new Intent(Splash_Screen.this, Choose_Language.class));
                    finish();
                }else{
                   // Log.d("logginse","loginfailure");
                    startActivity(new Intent(Splash_Screen.this, HomeBottomNavigation.class));
                    finish();
            }
            }
        },3000);

    }

}
