package com.vamosys.drs.driver.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.vamosys.drs.driver.ApiClient.ApiClient;
import com.vamosys.drs.driver.ApiClient.ApiInterface;
import com.vamosys.drs.driver.ApiClient.Const;
import com.vamosys.drs.driver.CheckListPereference;
import com.vamosys.drs.driver.R;

import okhttp3.ResponseBody;
import retrofit2.Call;

public class AddCheckList1 extends AppCompatActivity {
    // saftey
    Button safok, safnotok, safremark;

    //break
    Button breok, brenotok, breremark;

    //engine
    Button engok, engnotok, engremark;

    //trans
    Button transok, transnotok, transremark;

    //greece
    Button greok, grenotok, greremark;

    //wipers
    Button wipok, wipnotok, wipremark;

    //beam
    Button beamok, beamnotok, beamremark;

    Button btn_checlist;

    CheckListPereference pereference;
    ApiInterface apiInterface;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_check_list1);

        pereference= new CheckListPereference(AddCheckList1.this);
        //safety
        safok = findViewById(R.id.btn_safok);
        safnotok = findViewById(R.id.btn_safnotok);
        safremark = findViewById(R.id.btn_safrem);

        // break
        breok = findViewById(R.id.btn_brakeok);
        brenotok = findViewById(R.id.btn_brakenotok);
        breremark = findViewById(R.id.btn_brakeremark);

        // engine
        engok = findViewById(R.id.btn_engineok);
        engnotok = findViewById(R.id.btn_enginenotok);
        engremark = findViewById(R.id.btn_engineremark);

        // transmission
        transok = findViewById(R.id.btn_transok);
        transnotok = findViewById(R.id.btn_transnotok);
        transremark = findViewById(R.id.btn_transremark);

        //greece
        greok = findViewById(R.id.btn_greeceok);
        grenotok = findViewById(R.id.btn_greecenotok);
        greremark = findViewById(R.id.btn_greeceremark);

        // wipers
        wipok = findViewById(R.id.btn_wiperok);
        wipnotok = findViewById(R.id.btn_wipernotok);
        wipremark = findViewById(R.id.btn_wiperremark);

        //beam
        beamok = findViewById(R.id.btn_beamok);
        beamnotok = findViewById(R.id.btn_beamnotok);
        beamremark = findViewById(R.id.btn_beamremark);
        btn_checlist = findViewById(R.id.btn_checklist);

        setSelectedData();

        btn_checlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i=new Intent(getApplicationContext(),AddCheckList2.class);
                startActivity(i);
            }
        });
        safok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                safok.setBackgroundResource(R.drawable.curve_pink);
                safok.setTextColor(Color.parseColor("#C41027"));

                safnotok.setBackgroundResource(R.drawable.curve_grey);
                safok.setTextColor(Color.parseColor("#000000"));

                safremark.setBackgroundResource(R.drawable.curve_grey);
                safremark.setTextColor(Color.parseColor("#000000"));
                pereference.setSafetyBelt("OK:-");
            }
        });

        safnotok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                safnotok.setBackgroundResource(R.drawable.curve_pink);
                safnotok.setTextColor(Color.parseColor("#C41027"));

                safok.setBackgroundResource(R.drawable.curve_grey);
                safok.setTextColor(Color.parseColor("#000000"));

                safremark.setBackgroundResource(R.drawable.curve_grey);
                safremark.setTextColor(Color.parseColor("#000000"));
                pereference.setSafetyBelt("NOTOK:-");
            }
        });

        safremark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                safremark.setBackgroundResource(R.drawable.curve_pink);
                safremark.setTextColor(Color.parseColor("#C41027"));

                safok.setBackgroundResource(R.drawable.curve_grey);
                safok.setTextColor(Color.parseColor("#000000"));

                safnotok.setBackgroundResource(R.drawable.curve_grey);
                safnotok.setTextColor(Color.parseColor("#000000"));
                pereference.setSafetyBelt("-:Remarks");
            }
        });

        breok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                breok.setBackgroundResource(R.drawable.curve_pink);
                breok.setTextColor(Color.parseColor("#C41027"));

                brenotok.setBackgroundResource(R.drawable.curve_grey);
                brenotok.setTextColor(Color.parseColor("#000000"));

                breremark.setBackgroundResource(R.drawable.curve_grey);
                breremark.setTextColor(Color.parseColor("#000000"));
                pereference.setBrake("OK:-");

            }
        });

        brenotok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                brenotok.setBackgroundResource(R.drawable.curve_pink);
                brenotok.setTextColor(Color.parseColor("#C41027"));

                breok.setBackgroundResource(R.drawable.curve_grey);
                breok.setTextColor(Color.parseColor("#000000"));

                breremark.setBackgroundResource(R.drawable.curve_grey);
                breremark.setTextColor(Color.parseColor("#000000"));
                pereference.setBrake("NOTOK:-");
            }
        });

        breremark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                breremark.setBackgroundResource(R.drawable.curve_pink);
                breremark.setTextColor(Color.parseColor("#C41027"));

                breok.setBackgroundResource(R.drawable.curve_grey);
                breok.setTextColor(Color.parseColor("#000000"));

                brenotok.setBackgroundResource(R.drawable.curve_grey);
                brenotok.setTextColor(Color.parseColor("#000000"));
                pereference.setBrake("-:Remarks");
            }
        });


        engok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                engok.setBackgroundResource(R.drawable.curve_pink);
                engok.setTextColor(Color.parseColor("#C41027"));

                engnotok.setBackgroundResource(R.drawable.curve_grey);
                engnotok.setTextColor(Color.parseColor("#000000"));

                engremark.setBackgroundResource(R.drawable.curve_grey);
                engremark.setTextColor(Color.parseColor("#000000"));
                pereference.setEngine("OK:-");
            }
        });

        engnotok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                engnotok.setBackgroundResource(R.drawable.curve_pink);
                engnotok.setTextColor(Color.parseColor("#C41027"));

                engok.setBackgroundResource(R.drawable.curve_grey);
                engok.setTextColor(Color.parseColor("#000000"));

                engremark.setBackgroundResource(R.drawable.curve_grey);
                engremark.setTextColor(Color.parseColor("#000000"));
                pereference.setEngine("NOTOK:-");
            }
        });

        engremark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                engremark.setBackgroundResource(R.drawable.curve_pink);
                engremark.setTextColor(Color.parseColor("#C41027"));

                engok.setBackgroundResource(R.drawable.curve_grey);
                engok.setTextColor(Color.parseColor("#000000"));

                engnotok.setBackgroundResource(R.drawable.curve_grey);
                engnotok.setTextColor(Color.parseColor("#000000"));
                pereference.setEngine("-:Remark");
            }
        });

        transok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transok.setBackgroundResource(R.drawable.curve_pink);
                transok.setTextColor(Color.parseColor("#C41027"));

                transnotok.setBackgroundResource(R.drawable.curve_grey);
                transnotok.setTextColor(Color.parseColor("#000000"));

                transremark.setBackgroundResource(R.drawable.curve_grey);
                transremark.setTextColor(Color.parseColor("#000000"));

                pereference.setTransmission("OK:-");
            }
        });


        transnotok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transnotok.setBackgroundResource(R.drawable.curve_pink);
                transnotok.setTextColor(Color.parseColor("#C41027"));

                transok.setBackgroundResource(R.drawable.curve_grey);
                transok.setTextColor(Color.parseColor("#000000"));

                transremark.setBackgroundResource(R.drawable.curve_grey);

                pereference.setTransmission("NOTOK:-");
            }
        });

        transremark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transremark.setBackgroundResource(R.drawable.curve_pink);
                transremark.setTextColor(Color.parseColor("#C41027"));

                transok.setBackgroundResource(R.drawable.curve_grey);
                transok.setTextColor(Color.parseColor("#000000"));

                transnotok.setBackgroundResource(R.drawable.curve_grey);
                transnotok.setTextColor(Color.parseColor("#000000"));
                pereference.setTransmission("-:Remarks");
            }
        });

        greok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                greok.setBackgroundResource(R.drawable.curve_pink);
                greok.setTextColor(Color.parseColor("#C41027"));

                grenotok.setBackgroundResource(R.drawable.curve_grey);
                grenotok.setTextColor(Color.parseColor("#000000"));

                greremark.setBackgroundResource(R.drawable.curve_grey);
                greremark.setTextColor(Color.parseColor("#000000"));
                pereference.setGreecePacking("OK:-");
            }
        });

        grenotok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                grenotok.setBackgroundResource(R.drawable.curve_pink);
                grenotok.setTextColor(Color.parseColor("#C41027"));

                greok.setBackgroundResource(R.drawable.curve_grey);
                greok.setTextColor(Color.parseColor("#000000"));

                greremark.setBackgroundResource(R.drawable.curve_grey);
                greremark.setTextColor(Color.parseColor("#000000"));
                pereference.setGreecePacking("NOTOK:-");
            }
        });

        greremark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                greremark.setBackgroundResource(R.drawable.curve_pink);
                greremark.setTextColor(Color.parseColor("#C41027"));

                grenotok.setBackgroundResource(R.drawable.curve_grey);
                grenotok.setTextColor(Color.parseColor("#000000"));

                greok.setBackgroundResource(R.drawable.curve_grey);
                greok.setTextColor(Color.parseColor("#000000"));
                pereference.setGreecePacking("-:Remarks");
            }
        });


        wipok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wipok.setBackgroundResource(R.drawable.curve_pink);
                wipok.setTextColor(Color.parseColor("#C41027"));

                wipnotok.setBackgroundResource(R.drawable.curve_grey);
                wipnotok.setTextColor(Color.parseColor("#000000"));

                wipremark.setBackgroundResource(R.drawable.curve_grey);
                wipremark.setTextColor(Color.parseColor("#000000"));
                pereference.setWipers("OK:-");
            }
        });

        wipnotok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wipnotok.setBackgroundResource(R.drawable.curve_pink);
                wipnotok.setTextColor(Color.parseColor("#C41027"));

                wipok.setBackgroundResource(R.drawable.curve_grey);
                wipok.setTextColor(Color.parseColor("#000000"));

                wipremark.setBackgroundResource(R.drawable.curve_grey);
                wipremark.setTextColor(Color.parseColor("#000000"));
                pereference.setWipers("NOTOK:-");
            }
        });

        wipremark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wipremark.setBackgroundResource(R.drawable.curve_pink);
                wipremark.setTextColor(Color.parseColor("#C41027"));

                wipok.setBackgroundResource(R.drawable.curve_grey);
                wipok.setTextColor(Color.parseColor("#000000"));

                wipnotok.setBackgroundResource(R.drawable.curve_grey);
                wipnotok.setTextColor(Color.parseColor("#000000"));
                pereference.setWipers("-:Remarks");
            }
        });

        beamok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beamok.setBackgroundResource(R.drawable.curve_pink);
                beamok.setTextColor(Color.parseColor("#C41027"));

                beamnotok.setBackgroundResource(R.drawable.curve_grey);
                beamnotok.setTextColor(Color.parseColor("#000000"));

                beamremark.setBackgroundResource(R.drawable.curve_grey);
                beamremark.setTextColor(Color.parseColor("#000000"));
                pereference.setHighbeam("OK:-");
            }
        });

        beamnotok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beamnotok.setBackgroundResource(R.drawable.curve_pink);
                beamnotok.setTextColor(Color.parseColor("#C41027"));

                beamok.setBackgroundResource(R.drawable.curve_grey);
                beamok.setTextColor(Color.parseColor("#000000"));

                beamremark.setBackgroundResource(R.drawable.curve_grey);
                beamremark.setTextColor(Color.parseColor("#000000"));
                pereference.setHighbeam("NOTOK:-");
            }
        });

        beamremark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beamremark.setBackgroundResource(R.drawable.curve_pink);
                beamremark.setTextColor(Color.parseColor("#C41027"));

                beamok.setBackgroundResource(R.drawable.curve_grey);
                beamok.setTextColor(Color.parseColor("#000000"));

                beamnotok.setBackgroundResource(R.drawable.curve_grey);
                beamnotok.setTextColor(Color.parseColor("#000000"));
                pereference.setHighbeam("-:Remarks");
            }
        });
    }

    private void setSelectedData(){
        setBackgroundButton(safok,safnotok,safremark,pereference.getSateft_belt());
        setBackgroundButton(breok,brenotok,breremark,pereference.getBrake());
        setBackgroundButton(engok,engnotok,engremark,pereference.getEngine());
        setBackgroundButton(transok,transnotok,transremark,pereference.getTransmission());
        setBackgroundButton(greok,grenotok,greremark,pereference.getGreecePacking());
        setBackgroundButton(wipok,wipnotok,wipremark,pereference.getWipers());
        setBackgroundButton(beamok,beamnotok,beamremark,pereference.getHighbeam());
    }
    private void setBackgroundButton(Button btn1,Button btn2,Button btn3, String click_val){
        if(click_val.equals("OK:-")){
            btn1.setBackgroundResource(R.drawable.curve_pink);
            btn2.setBackgroundResource(R.drawable.curve_grey);
            btn3.setBackgroundResource(R.drawable.curve_grey);
        }
        else if(click_val.equals("NOTOK:-")){
            btn1.setBackgroundResource(R.drawable.curve_grey);
            btn2.setBackgroundResource(R.drawable.curve_pink);
            btn3.setBackgroundResource(R.drawable.curve_grey);
        }
        else if(click_val.equals("-:Remarks")) {
            btn1.setBackgroundResource(R.drawable.curve_grey);
            btn2.setBackgroundResource(R.drawable.curve_grey);
            btn3.setBackgroundResource(R.drawable.curve_pink);
        }

    }
}
