package com.vamosys.drs.driver.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Bundle;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.vamosys.drs.driver.ApiClient.ApiClient;
import com.vamosys.drs.driver.ApiClient.ApiInterface;
import com.vamosys.drs.driver.ApiClient.Const;
import com.vamosys.drs.driver.AppPreference;
import com.vamosys.drs.driver.HomeBottomNavigation;
import com.vamosys.drs.driver.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.vamosys.drs.driver.Services.GoogleService;
import com.vamosys.drs.driver.Services.MyService;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.preference.PreferenceManager;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends LocalizationActivity implements View.OnClickListener {
    AppPreference appPreference;
    private Button heavy_vehicle, truck_diving, loading, unloading, diving_truck, more, exp1, exp2, exp3, exp4, exp5, exp6;
    private Button tamil, telugu, hindi, social, website, advertice, submitt, english, kannada, marati, bengali, gujarati;
    private boolean isclicked = false, isexp1 = true,isexp2 = false,isexp3 = false,isexp4 = false,isexp5 = false,isexp6= false,istamil = true, isteligu = false, ishindi = false, isenglish = false, iskanada = false, ismarati = false, isbengali = false, isgujarati = false;
    ApiInterface apiInterface;
    private boolean twele = true, eighteen = false;
    private boolean one = true, six = false, ten = false, twenty = false, tsix = false, thirty = false;
    private boolean soc = true, web = false, adver = false;
    SharedPreferences loginprefer;
    SharedPreferences.Editor logiedit;
    FirebaseFirestore db;
    String languageknow;
    String skills, about, experience;
    public static final int REQUEST_CODE_PERMISSIONS = 101;
    private JobScheduler jobScheduler;
    private ComponentName componentName;
    private JobInfo jobInfo;

    private static final int REQUEST_PERMISSIONS = 100;
    boolean boolean_permission;
    SharedPreferences mPref;
    SharedPreferences.Editor medit;
    Double latitude,longitude;
    Geocoder geocoder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        db = FirebaseFirestore.getInstance();
        loginprefer = getSharedPreferences("login_session", Context.MODE_PRIVATE);
        logiedit = loginprefer.edit();
        appPreference = new AppPreference(this);
        System.out.println("ggg" + appPreference.getLicense() + appPreference.getDob() + appPreference.getAddress() + appPreference.getPrevious_Company() + appPreference.getLicenseNUMBER() + appPreference.getAadharnumber());
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        if (appPreference.getLangName().equalsIgnoreCase("english")) {
            setLanguage("en");
        }
        if (appPreference.getLangName().equalsIgnoreCase("hindi")) {
            setLanguage("hi");
        }
        findviewid();

        geocoder = new Geocoder(this, Locale.getDefault());
        mPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        medit = mPref.edit();
        fn_permission();


    }

    private void fn_permission() {
        if ((ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {

            if ((ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION))) {


            } else {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION

                        },
                        REQUEST_PERMISSIONS);

            }
        } else {
            boolean_permission = true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_PERMISSIONS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    boolean_permission = true;

                } else {
                    Toast.makeText(getApplicationContext(), "Please allow the permission", Toast.LENGTH_LONG).show();

                }
            }
        }
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            latitude = Double.valueOf(intent.getStringExtra("latutide"));
            longitude = Double.valueOf(intent.getStringExtra("longitude"));

            List<Address> addresses = null;

            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
                String cityName = addresses.get(0).getAddressLine(0);
                String stateName = addresses.get(0).getAddressLine(1);
                String countryName = addresses.get(0).getAddressLine(2);

//                tv_area.setText(addresses.get(0).getAdminArea());
//                tv_locality.setText(stateName);
//                tv_address.setText(countryName);



            } catch (IOException e1) {
                e1.printStackTrace();
            }


//            tv_latitude.setText(latitude+"");
//            tv_longitude.setText(longitude+"");
//            tv_address.getText();


        }
    };


    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }


    private void findviewid() {
        heavy_vehicle = findViewById(R.id.btn_heavy);
        truck_diving = findViewById(R.id.btn_truck);

        exp1 = findViewById(R.id.btn_exp1);
        exp2 = findViewById(R.id.btn_exp2);
        exp3 = findViewById(R.id.btn_exp3);
        exp4 = findViewById(R.id.btn_exp4);
        exp5 = findViewById(R.id.btn_exp5);
        exp6 = findViewById(R.id.btn_exp6);
        tamil = findViewById(R.id.btb_tamil);
        telugu = findViewById(R.id.btb_telugu);
        hindi = findViewById(R.id.btb_hindi);
        social = findViewById(R.id.btb_social);
        website = findViewById(R.id.btb_website);
        advertice = findViewById(R.id.btb_advertice);
        submitt = findViewById(R.id.submit);
        english = findViewById(R.id.btb_english);
        kannada = findViewById(R.id.btb_kannada);
        marati = findViewById(R.id.btb_marati);
        bengali = findViewById(R.id.btb_bengali);
        gujarati = findViewById(R.id.btb_gujarati);

        heavy_vehicle.setOnClickListener(this);
        truck_diving.setOnClickListener(this);
        //  loading.setOnClickListener(this);
        //unloading.setOnClickListener(this);
        english.setOnClickListener(this);
        kannada.setOnClickListener(this);
        marati.setOnClickListener(this);
        bengali.setOnClickListener(this);
        gujarati.setOnClickListener(this);
        exp1.setOnClickListener(this);
        exp2.setOnClickListener(this);
        exp3.setOnClickListener(this);
        exp4.setOnClickListener(this);
        exp5.setOnClickListener(this);
        exp6.setOnClickListener(this);
        tamil.setOnClickListener(this);
        telugu.setOnClickListener(this);
        hindi.setOnClickListener(this);
        social.setOnClickListener(this);
        website.setOnClickListener(this);
        advertice.setOnClickListener(this);
        submitt.setOnClickListener(this);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_heavy:
/* case R.id.btb_tamil:

                if (istamil == false) {
                    tamil.setBackgroundResource(R.drawable.curve_background_green);
                    tamil.setTextColor(Color.parseColor("#FFFFFF"));
                    istamil = true;

                } else {
                    tamil.setBackgroundResource(R.drawable.curve_background_white);
                    tamil.setTextColor(Color.parseColor("#000000"));
                    istamil = false;
                }

                break;*/

                if (twele == false) {
                    heavy_vehicle.setBackgroundResource(R.drawable.curve_background_green);
                    heavy_vehicle.setTextColor(Color.parseColor("#FFFFFF"));
                    twele = true;
                } else {
                    heavy_vehicle.setBackgroundResource(R.drawable.curve_background_white);
                    heavy_vehicle.setTextColor(Color.parseColor("#000000"));
                    twele = false;
                }
                break;
            case R.id.btn_truck:
                if (eighteen == false) {
                    truck_diving.setBackgroundResource(R.drawable.curve_background_green);
                    truck_diving.setTextColor(Color.parseColor("#FFFFFF"));
                    eighteen = true;
                } else {
                    truck_diving.setBackgroundResource(R.drawable.curve_background_white);
                    truck_diving.setTextColor(Color.parseColor("#000000"));
                    eighteen = false;
                }
                break;


            case R.id.btn_exp1:
                  if (isexp1 == false) {
                      exp1.setBackgroundResource(R.drawable.curve_background_green);
                      exp1.setTextColor(Color.parseColor("#FFFFFF"));
                      one = true;

                      exp2.setBackgroundResource(R.drawable.curve_background_white);
                      exp3.setBackgroundResource(R.drawable.curve_background_white);
                      exp4.setBackgroundResource(R.drawable.curve_background_white);
                      exp5.setBackgroundResource(R.drawable.curve_background_white);
                      exp6.setBackgroundResource(R.drawable.curve_background_white);
                      exp2.setTextColor(Color.parseColor("#000000"));
                      exp3.setTextColor(Color.parseColor("#000000"));
                      exp4.setTextColor(Color.parseColor("#000000"));
                      exp5.setTextColor(Color.parseColor("#000000"));
                      exp6.setTextColor(Color.parseColor("#000000"));
                      experience = "1-5";
                      isexp1 = true;
                  }else {
                      exp1.setBackgroundResource(R.drawable.curve_background_green);
                      exp1.setTextColor(Color.parseColor("#FFFFFF"));
                      one = true;

                      exp2.setBackgroundResource(R.drawable.curve_background_white);
                      exp3.setBackgroundResource(R.drawable.curve_background_white);
                      exp4.setBackgroundResource(R.drawable.curve_background_white);
                      exp5.setBackgroundResource(R.drawable.curve_background_white);
                      exp6.setBackgroundResource(R.drawable.curve_background_white);
                      exp2.setTextColor(Color.parseColor("#000000"));
                      exp3.setTextColor(Color.parseColor("#000000"));
                      exp4.setTextColor(Color.parseColor("#000000"));
                      exp5.setTextColor(Color.parseColor("#000000"));
                      exp6.setTextColor(Color.parseColor("#000000"));
                      experience = "1-5";
                      isexp1 = false;
                  }

                break;

            case R.id.btn_exp2:
                    exp2.setBackgroundResource(R.drawable.curve_background_green);
                    exp2.setTextColor(Color.parseColor("#FFFFFF"));
                    six = true;

                    exp1.setBackgroundResource(R.drawable.curve_background_white);
                    exp3.setBackgroundResource(R.drawable.curve_background_white);
                    exp4.setBackgroundResource(R.drawable.curve_background_white);
                    exp5.setBackgroundResource(R.drawable.curve_background_white);
                    exp6.setBackgroundResource(R.drawable.curve_background_white);
                    exp1.setTextColor(Color.parseColor("#000000"));
                    exp3.setTextColor(Color.parseColor("#000000"));
                    exp4.setTextColor(Color.parseColor("#000000"));
                    exp5.setTextColor(Color.parseColor("#000000"));
                    exp6.setTextColor(Color.parseColor("#000000"));
                    experience = "5-10";
                break;
            case R.id.btn_exp3:

                exp3.setBackgroundResource(R.drawable.curve_background_green);
                exp3.setTextColor(Color.parseColor("#FFFFFF"));
                ten = true;

                exp1.setBackgroundResource(R.drawable.curve_background_white);
                exp2.setBackgroundResource(R.drawable.curve_background_white);
                exp4.setBackgroundResource(R.drawable.curve_background_white);
                exp5.setBackgroundResource(R.drawable.curve_background_white);
                exp6.setBackgroundResource(R.drawable.curve_background_white);
                exp2.setTextColor(Color.parseColor("#000000"));
                exp1.setTextColor(Color.parseColor("#000000"));
                exp4.setTextColor(Color.parseColor("#000000"));
                exp5.setTextColor(Color.parseColor("#000000"));
                exp6.setTextColor(Color.parseColor("#000000"));

                experience = "10-15";
                break;
            case R.id.btn_exp4:

                exp4.setBackgroundResource(R.drawable.curve_background_green);
                exp4.setTextColor(Color.parseColor("#FFFFFF"));
                twenty = true;

                exp1.setBackgroundResource(R.drawable.curve_background_white);
                exp2.setBackgroundResource(R.drawable.curve_background_white);
                exp3.setBackgroundResource(R.drawable.curve_background_white);
                exp5.setBackgroundResource(R.drawable.curve_background_white);
                exp6.setBackgroundResource(R.drawable.curve_background_white);
                exp2.setTextColor(Color.parseColor("#000000"));
                exp1.setTextColor(Color.parseColor("#000000"));
                exp3.setTextColor(Color.parseColor("#000000"));
                exp5.setTextColor(Color.parseColor("#000000"));
                exp6.setTextColor(Color.parseColor("#000000"));


                experience = "15-20";
                break;
            case R.id.btn_exp5:

                exp5.setBackgroundResource(R.drawable.curve_background_green);
                exp5.setTextColor(Color.parseColor("#FFFFFF"));
                tsix = true;

                exp1.setBackgroundResource(R.drawable.curve_background_white);
                exp2.setBackgroundResource(R.drawable.curve_background_white);
                exp3.setBackgroundResource(R.drawable.curve_background_white);
                exp4.setBackgroundResource(R.drawable.curve_background_white);
                exp6.setBackgroundResource(R.drawable.curve_background_white);
                exp2.setTextColor(Color.parseColor("#000000"));
                exp1.setTextColor(Color.parseColor("#000000"));
                exp3.setTextColor(Color.parseColor("#000000"));
                exp4.setTextColor(Color.parseColor("#000000"));
                exp6.setTextColor(Color.parseColor("#000000"));

                experience = "20-25";
                break;
            case R.id.btn_exp6:

                exp6.setBackgroundResource(R.drawable.curve_background_green);
                exp6.setTextColor(Color.parseColor("#FFFFFF"));

                exp1.setBackgroundResource(R.drawable.curve_background_white);
                exp2.setBackgroundResource(R.drawable.curve_background_white);
                exp3.setBackgroundResource(R.drawable.curve_background_white);
                exp4.setBackgroundResource(R.drawable.curve_background_white);
                exp5.setBackgroundResource(R.drawable.curve_background_white);
                exp2.setTextColor(Color.parseColor("#000000"));
                exp1.setTextColor(Color.parseColor("#000000"));
                exp3.setTextColor(Color.parseColor("#000000"));
                exp4.setTextColor(Color.parseColor("#000000"));
                exp5.setTextColor(Color.parseColor("#000000"));

                experience = "25-30";
                break;

            case R.id.btb_tamil:

                if (istamil == false) {
                    tamil.setBackgroundResource(R.drawable.curve_background_green);
                    tamil.setTextColor(Color.parseColor("#FFFFFF"));
                    istamil = true;

                } else {
                    tamil.setBackgroundResource(R.drawable.curve_background_white);
                    tamil.setTextColor(Color.parseColor("#000000"));
                    istamil = false;
                }

                break;


            case R.id.btb_telugu:

                if (isteligu == false) {
                    telugu.setBackgroundResource(R.drawable.curve_background_green);
                    telugu.setTextColor(Color.parseColor("#FFFFFF"));
                    isteligu = true;
                } else {
                    telugu.setBackgroundResource(R.drawable.curve_background_white);
                    telugu.setTextColor(Color.parseColor("#000000"));
                    isteligu = false;
                }


                break;

            case R.id.btb_hindi:
                if (ishindi == false) {
                    hindi.setBackgroundResource(R.drawable.curve_background_green);
                    hindi.setTextColor(Color.parseColor("#FFFFFF"));
                    ishindi = true;
                } else {
                    hindi.setBackgroundResource(R.drawable.curve_background_white);
                    hindi.setTextColor(Color.parseColor("#000000"));
                    ishindi = false;
                }


                break;

            case R.id.btb_english:

                if (isenglish == false) {
                    english.setBackgroundResource(R.drawable.curve_background_green);
                    english.setTextColor(Color.parseColor("#FFFFFF"));
                    isenglish = true;
                } else {
                    english.setBackgroundResource(R.drawable.curve_background_white);
                    english.setTextColor(Color.parseColor("#000000"));
                    isenglish = false;
                }


                break;


            case R.id.btb_kannada:

                if (iskanada == false) {
                    kannada.setBackgroundResource(R.drawable.curve_background_green);
                    kannada.setTextColor(Color.parseColor("#FFFFFF"));
                    iskanada = true;

                } else {
                    kannada.setBackgroundResource(R.drawable.curve_background_white);
                    kannada.setTextColor(Color.parseColor("#000000"));
                    iskanada = false;
                }


                break;

            case R.id.btb_marati:

                if (ismarati == false) {
                    marati.setBackgroundResource(R.drawable.curve_background_green);
                    marati.setTextColor(Color.parseColor("#FFFFFF"));
                    ismarati = true;
                } else {
                    marati.setBackgroundResource(R.drawable.curve_background_white);
                    marati.setTextColor(Color.parseColor("#000000"));
                    ismarati = false;
                }


                break;

            case R.id.btb_bengali:

                if (isbengali == false) {
                    bengali.setBackgroundResource(R.drawable.curve_background_green);
                    bengali.setTextColor(Color.parseColor("#FFFFFF"));
                    isbengali = true;
                } else {
                    bengali.setBackgroundResource(R.drawable.curve_background_white);
                    bengali.setTextColor(Color.parseColor("#000000"));
                    isbengali = false;
                }


                break;

            case R.id.btb_gujarati:

                if (isgujarati == false) {
                    gujarati.setBackgroundResource(R.drawable.curve_background_green);
                    gujarati.setTextColor(Color.parseColor("#FFFFFF"));
                    isgujarati = true;
                } else {
                    gujarati.setBackgroundResource(R.drawable.curve_background_white);
                    gujarati.setTextColor(Color.parseColor("#000000"));
                    isgujarati = false;
                }

                break;

            case R.id.btb_social:

                social.setBackgroundResource(R.drawable.curve_background_green);
                social.setTextColor(Color.parseColor("#FFFFFF"));

                website.setBackgroundResource(R.drawable.curve_background_white);
                advertice.setBackgroundResource(R.drawable.curve_background_white);
                website.setTextColor(Color.parseColor("#000000"));
                advertice.setTextColor(Color.parseColor("#000000"));
                about = "Social Media";


                break;
            case R.id.btb_website:
                website.setBackgroundResource(R.drawable.curve_background_green);
                website.setTextColor(Color.parseColor("#FFFFFF"));
                social.setBackgroundResource(R.drawable.curve_background_white);
                advertice.setBackgroundResource(R.drawable.curve_background_white);
                social.setTextColor(Color.parseColor("#000000"));
                advertice.setTextColor(Color.parseColor("#000000"));
                about = "Website";
                break;

            case R.id.btb_advertice:
                advertice.setBackgroundResource(R.drawable.curve_background_green);
                advertice.setTextColor(Color.parseColor("#FFFFFF"));
                website.setBackgroundResource(R.drawable.curve_background_white);
                social.setBackgroundResource(R.drawable.curve_background_white);
                website.setTextColor(Color.parseColor("#000000"));
                social.setTextColor(Color.parseColor("#000000"));
                about = "Advertisment";
                break;

            case R.id.submit:
                gotoValidation();
                break;

            default:
                break;
        }
    }

    private void gotoValidation() {
        System.out.println("license" + appPreference.getLicense());
        System.out.println("dob" + appPreference.getDob());
        System.out.println("address" + appPreference.getAddress());
        System.out.println("previous" + appPreference.getPrevious_Company());
        System.out.println("name" + appPreference.getAccName());
        System.out.println("phone" + appPreference.getPhone());
        System.out.println("hub" + appPreference.getHub());


        String name = appPreference.getAccName();
        String hub = appPreference.getHub();
        String address = appPreference.getAddress();
        String phone = appPreference.getPhone();
        String licence = appPreference.getLicense();
        String dob = appPreference.getDob();
        String previous = appPreference.getPrevious_Company();
        languageknow =null;
        languageknow = istamil ? "Tamil" : languageknow;
        languageknow = isteligu ? languageknow + ",Telugu" : languageknow;
        languageknow = ishindi ? languageknow + ",Hindi" : languageknow;
        languageknow = isenglish ? languageknow + ",English" : languageknow;
        languageknow = iskanada ? languageknow + ",Kannada" : languageknow;
        languageknow = ismarati ? languageknow + ",Marati" : languageknow;
        languageknow = isbengali ? languageknow + ",Bengali" : languageknow;
        languageknow = isgujarati ? languageknow + ",Gujarati" : languageknow;

        //experience

    /*    experience = one ? "1-5" : experience;
        experience = six ? experience + ",5-10" : experience;
        experience = ten ? experience + ",10-15" : experience;
        experience = twenty ? experience + ",20-25" : experience;
        experience = tsix ? experience + ",25-30" : experience;
        experience = thirty ? experience + ",35-40" : experience;*/
//about
      /*  experience = twenty ? experience + ",20-25" : experience;
        experience = tsix ? experience + ",25-30" : experience;
        experience = thirty ? experience + ",35-40" : experience;*/
        System.out.println("laguageknown  " + languageknow);
       // experience = isexp1 ? "1-5" : experience;
        if (experience == null){
            experience = "1-5";
        }
        if (about == null){
            about = getResources().getString(R.string.social);
        }

        // skills


        skills = twele ? "12 Wheeler" : skills;
        skills = eighteen ? skills + ",18 Wheeler" : skills;
        System.out.println("mainUUid  " + appPreference.getUuid());
        System.out.println("mainmobile  " + Const.Mobile_number);
        System.out.println("exrr  " + experience);
        System.out.println("accccccc   " + appPreference.getAccName());
        System.out.println("hubb   " + appPreference.getHub());
        System.out.println("addds  " + appPreference.getAddress());
        System.out.println("phoness  " + appPreference.getPhone());
        System.out.println("liensee  " + appPreference.getLicense());
        System.out.println("previousss   " + appPreference.getPrevious_Company());
        System.out.println("licenecenumberr  " + appPreference.getLicenseNUMBER());
        System.out.println("aadhhharrr " + appPreference.getAadharnumber());
        System.out.println("dooobbbb  " + appPreference.getDob());
        System.out.println("skillssss"+skills);
        System.out.println("about"+about);

      if (skills == null){
          Toast.makeText(this, R.string.toast_skills, Toast.LENGTH_SHORT).show();

      } else if (languageknow == null){
            Toast.makeText(this, R.string.toast_lang, Toast.LENGTH_SHORT).show();

        }
      else {

          Call<ResponseBody> call = apiInterface.send_value(appPreference.getUuid(), appPreference.getAccName(), appPreference.getHub(), "23", appPreference.getAddress(), appPreference.getPhone(), appPreference.getLicense(), "", "", "", "", "", "", "", previous, "", "", "", "", "", "", experience, "", "", languageknow, "", appPreference.getLicenseNUMBER(), appPreference.getAadharnumber(), "", "", "", "", "", "", dob,skills,about);

          System.out.println("hhhh   " + call.request().url().toString());
          call.enqueue(new Callback<ResponseBody>() {
              @Override
              public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                  logiedit.putBoolean("is_loggin", true);
                  logiedit.putString("mobilenumber",appPreference.getPhone());
                  logiedit.commit();
                  System.out.println("Mobilenumbermain  " + Const.Mobile_number);
                  savenumber(appPreference.getPhone());
                  startActivity(new Intent(MainActivity.this, HomeBottomNavigation.class));
                  if (boolean_permission) {

                      if (mPref.getString("service", "").matches("")) {
                          medit.putString("service", "service").commit();

                          Intent intent = new Intent(getApplicationContext(), HomeBottomNavigation.class);
                          startService(intent);

                      } else {
                          Toast.makeText(getApplicationContext(), "Service is already running", Toast.LENGTH_SHORT).show();
                      }
                  } else {
                      Toast.makeText(getApplicationContext(), "Please enable the gps", Toast.LENGTH_SHORT).show();
                  }
              }

              @Override
              public void onFailure(Call<ResponseBody> call, Throwable t) {

              }
          });

      }


        }

//    private boolean requestLocationPermission() {
//        if(!checkpermission()) {
//            ActivityCompat.requestPermissions(this,
//                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION
//                            ,Manifest.permission.SYSTEM_ALERT_WINDOW}, REQUEST_CODE_PERMISSIONS);
//        }
//        return true;
//    }
//
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if (requestCode == REQUEST_CODE_PERMISSIONS) {
//            boolean coarseLoc = grantResults[0]==PackageManager.PERMISSION_GRANTED;
//            boolean backLoc = grantResults[1]==PackageManager.PERMISSION_GRANTED;
//            boolean fineLoc = grantResults[2]==PackageManager.PERMISSION_GRANTED;
//            boolean man = grantResults[3]==PackageManager.PERMISSION_GRANTED;
//        }
//    }
//
//    @SuppressLint("NewApi")
//    public void StartBackgroundTask() {
//        checkGpsEnableOrNot();
//        long milliseconds = 5 * 60000;
//        jobScheduler = (JobScheduler) getApplicationContext().getSystemService(JOB_SCHEDULER_SERVICE);
//        componentName = new ComponentName(getApplicationContext(), MyService.class);
//        jobInfo = new JobInfo.Builder(1, componentName)
//                .setMinimumLatency(10000) //5 mins interval
//                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY).setRequiresCharging(false).build();
//        jobScheduler.schedule(jobInfo);
//    }
//
//    public boolean checkpermission(){
//        boolean foreground = ActivityCompat.checkSelfPermission(this,
//                Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
//
//        boolean fineLoc = ActivityCompat.checkSelfPermission(this,
//                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
//
////        boolean background = ActivityCompat.checkSelfPermission(this,
////                Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED;
//
//        return true==foreground&&fineLoc;
//    }
//
//    public void checkGpsEnableOrNot(){
//        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
//
//        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
//            Toast.makeText(this, "GPS is Enabled in your devide", Toast.LENGTH_SHORT).show();
//        }else{
//            showGPSDisabledAlertToUser();
//        }
//    }
//
//    public void showGPSDisabledAlertToUser(){
//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
//        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
//                .setCancelable(false)
//                .setPositiveButton("Goto Settings Page To Enable GPS",
//                        new DialogInterface.OnClickListener(){
//                            public void onClick(DialogInterface dialog, int id){
//                                Intent callGPSSettingIntent = new Intent(
//                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                                startActivity(callGPSSettingIntent);
//                            }
//                        });
//        alertDialogBuilder.setNegativeButton("Cancel",
//                new DialogInterface.OnClickListener(){
//                    public void onClick(DialogInterface dialog, int id){
//                        dialog.cancel();
//                    }
//                });
//        AlertDialog alert = alertDialogBuilder.create();
//        alert.show();
//    }




    private void savenumber(String mobile_number) {
        Map<String, Object> newnumber = new HashMap<>();
        newnumber.put("mobilenumber", mobile_number);
        newnumber.put("Role", "Driver");
        newnumber.put("Uuid",appPreference.getUuid());
        CollectionReference user = db.collection("Users");

        user.add(newnumber).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                // Toast.makeText(MainActivity.this, "User added", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                //Toast.makeText(MainActivity.this, "User not added", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver, new IntentFilter(GoogleService.str_receiver));
    }
}
