package com.vamosys.drs.driver;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.akexorcist.localizationactivity.ui.LocalizationActivity;
import com.vamosys.drs.driver.Activity.Lang_Row;

public class Choose_Language extends LocalizationActivity {
    RelativeLayout rl_hindi, rl_tamil, rl_telugu, rl_malayalam;
    ImageView img_hin, img_tam, img_tel, img_mal;
    AppPreference appPreference;
    Lang_Row lang_row;
    RecyclerView rv_lang;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose__language);
        appPreference = new AppPreference(this);
        rv_lang = findViewById(R.id.rv_lang);
        rv_lang.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
        rv_lang.setLayoutManager(layoutManager);
        getLang();
        System.out.println("gwjeh"+appPreference.getLangName());
       /* rl_hindi = findViewById(R.id.rl_hindi);
        rl_tamil = findViewById(R.id.rl_tamil);
        rl_telugu = findViewById(R.id.rl_telugu);
        rl_malayalam = findViewById(R.id.rl_malayalam);
        img_hin = findViewById(R.id.img_hin);
        img_mal = findViewById(R.id.img_mal);
        img_tam = findViewById(R.id.img_tam);
        img_tel = findViewById(R.id.imggtel);*/
 /*       rl_malayalam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_mal.setVisibility(View.VISIBLE);
                img_hin.setVisibility(View.GONE);
                img_tam.setVisibility(View.GONE);
                img_tel.setVisibility(View.GONE);
            }
        });
        rl_telugu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                img_mal.setVisibility(View.GONE);
                img_hin.setVisibility(View.GONE);
                img_tam.setVisibility(View.GONE);
                img_tel.setVisibility(View.VISIBLE);

            }
        });
        rl_tamil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                img_mal.setVisibility(View.GONE);
                img_hin.setVisibility(View.GONE);
                img_tam.setVisibility(View.VISIBLE);
                img_tel.setVisibility(View.GONE);
            }
        });
        rl_hindi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                img_mal.setVisibility(View.GONE);
                img_hin.setVisibility(View.VISIBLE);
                img_tam.setVisibility(View.GONE);
                img_tel.setVisibility(View.GONE);
                setLanguage("hi");
                appPreference.setLangName("hindi");
                Intent i=new Intent(getApplicationContext(), RegistrationActivity.class);
                startActivity(i);*/
        /*    }
        });*/

    }

    public void getLang() {
        lang_row = new Lang_Row(getApplicationContext(), 1);
        rv_lang.setAdapter(lang_row);
        if (appPreference.getLangName().equalsIgnoreCase("english")) {
            setLanguage("en");
        }
        if (appPreference.getLangName().equalsIgnoreCase("hindi")) {
            setLanguage("hi");
        }
    }
}
