
package com.vamosys.drs.driver.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SpinnerHub {

    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("createdDate")
    @Expose
    private String createdDate;
    @SerializedName("contactPersonNumer")
    @Expose
    private String contactPersonNumer;
    @SerializedName("rentedOwnerName")
    @Expose
    private String rentedOwnerName;
    @SerializedName("hubName")
    @Expose
    private String hubName;
    @SerializedName("totalDriver")
    @Expose
    private Integer totalDriver;

    public String getHindi() {
        return hindi;
    }

    public void setHindi(String hindi) {
        this.hindi = hindi;
    }

    @SerializedName("hindi")
    @Expose
    private String hindi;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("state")
    @Expose
    private String state;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getContactPersonNumer() {
        return contactPersonNumer;
    }

    public void setContactPersonNumer(String contactPersonNumer) {
        this.contactPersonNumer = contactPersonNumer;
    }

    public String getRentedOwnerName() {
        return rentedOwnerName;
    }

    public void setRentedOwnerName(String rentedOwnerName) {
        this.rentedOwnerName = rentedOwnerName;
    }

    public String getHubName() {
        return hubName;
    }

    public void setHubName(String hubName) {
        this.hubName = hubName;
    }

    public Integer getTotalDriver() {
        return totalDriver;
    }

    public void setTotalDriver(Integer totalDriver) {
        this.totalDriver = totalDriver;
    }

}
