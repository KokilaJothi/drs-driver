package com.vamosys.drs.driver.ApiClient;

import com.vamosys.drs.driver.Response.NewTripResponse;
import com.vamosys.drs.driver.Response.SpinnerHub;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiInterface {
    @GET("viewHubs")
    Call<List<SpinnerHub>> getSpinnerHub();

    @GET("addLocation?")
    Call<ResponseBody>addlocation(@Query("driverId")String driverid,@Query("driverName")String drivername,
                                  @Query("mobNum")String mobilenumber,@Query("time")String time,@Query("latitude")String latitude,@Query("longitude")String longitude);

    @GET("storeException?")
    Call<ResponseBody>getstoreexception(@Query("userId")String userid,@Query("reason")String reason,@Query("exception")String exception,@Query("mobileNum")String moblenuber);


    @GET("addDriver?")
    Call<ResponseBody> send_value(@Query("userId") String userId,
                                  @Query("driverName") String driverName,
                                  @Query("hubName") String hubName,
                                  @Query("age") String age,
                                  @Query("address") String address,
                                  @Query("mobileNo") String mobileNo,
                                  @Query("licenceNum") String licenceNum,
                                  @Query("aadharNum") String aadharNum, @Query("licenceValidity") String licenceValidity, @Query("maintenance") String maintenance,
                                  @Query("mechanism") String mechanism,
                                  @Query("reference") String reference,
                                  @Query("androidMobile") String androidMobile,
                                  @Query("bankDetails") String bankDetails,
                                  @Query("previousCompany") String previousCompany,
                                  @Query("familyDetails") String familyDetails,
                                  @Query("dailyFoodAllowance") String dailyFoodAllowance,
                                  @Query("mileageAchieved") String mileageAchieved,
                                  @Query("turnAroundTime") String turnAroundTime,
                                  @Query("attendanceBonus") String attendanceBonus,
                                  @Query("performance") String performance,
                                  @Query("experience") String experience,
                                  @Query("nativePlace") String nativePlace,
                                  @Query("salary") String salary,
                                  @Query("languagesKnown") String languagesKnown,
                                  @Query("backGroundDetails") String backGroundDetails,
                                  @Query("licenceFileName") String licenceFileName,
                                  @Query("adharFileName") String adharFileName,
                                  @Query("panNumber") String panNumber,
                                  @Query("panPhotoLocation") String panPhotoLocation,
                                  @Query("accountName") String accountName,
                                  @Query("accountType") String accountType,
                                  @Query("accountNumber") String accountNumber,
                                  @Query("payeeAccountName") String payeeAccountName,
                                  @Query("dob") String dob,
                                  @Query("skills") String skills, @Query("aboutUs") String aboutUs);

    //image upload
   /* @Multipart
    @POST("upload")
    Call<RequestBody> uploadImage(@Part("image\"; filename=\"myfile.jpg\" ") RequestBody file);*/


    @Multipart
    @POST("upload")
    Call<ResponseBody> uploadimage(@Part MultipartBody.Part uploadFiles);

    @GET("driverTakeActionForNewTrip?")
    Call<String> driverTakeActionForNewTrip(
            @Query("driverId")String driverid, @Query("vehicleId")String vehicleId,
            @Query("status")String status,@Query("seqNum")String seqNum,@Query("reason")String reason);

//    http://209.97.163.4:9010/getNewTripForDriver?&driverId=1&vehicleId=PAMUSA01
    @GET("getNewTripForDriver")
    Call<List<NewTripResponse>> getNewTrip(
            @Query("driverId") String driverId,
            @Query("vehicleId") String vehicleId
    );

//    http://209.97.163.4:9010/registrationBasedOnMobNum?mobNo=911111100000
    @POST("registrationBasedOnMobNum")
    Call<ResponseBody> registerMobileNum(
            @Query("mobNo") String MobileNumber
    );
//    http://209.97.163.4:9010/vehicleCheckList?vehicleId=PAMUSA01&safetyBelts=OK:-&brakesSteering=OK:-&engine=OK:-&transmission=OK:-&greecePacking=OK:-&wipers=OK:-&headLightsHighBeam=OK:-&headLightsLowBeam=OK:-&turnSignals=OK:-&brakeLights=OK:-&doors=OK:-&windows=OK:-&radio=OK:-&horn=OK:-&tiresTreadCondition=OK:-&tiresProperInfaltion=OK:-&lug=OK:-&fireExtinguisher=OK:-&firstAidKit=OK:-&accidentInformation=OK:-&liquidsLevelCheck=OK:-&radiator=OK:-&oil=OK:-&autoTransmission=OK:-&powerSteering=OK:-&brakes=OK:-&windowWasher=OK:-&driverId=raju&damageSection=YES&damageDesc=-
    @GET("vehicleCheckList")
    Call<ResponseBody> addVehicleCheckList(
            @Query("vehicleId") String vehicleId,
            @Query("safetyBelts") String safetyBelts,
            @Query("brakesSteering") String brakesSteering,
            @Query("engine") String engine,
            @Query("transmission") String transmission,
            @Query("greecePacking") String greecePacking,
            @Query("wipers") String wipers,
            @Query("headLightsHighBeam") String headLightsHighBeam,
            @Query("headLightsLowBeam") String headLightsLowBeam,
            @Query("turnSignals") String turnSignals,
            @Query("brakeLights") String brakeLights,
            @Query("doors") String doors,
            @Query("windows") String windows,
            @Query("radio") String radio,
            @Query("horn") String horn,
            @Query("tiresTreadCondition") String tiresTreadCondition,
            @Query("tiresProperInfaltion") String tiresProperInfaltion,
            @Query("lug") String lug,
            @Query("fireExtinguisher") String fireExtinguisher,
            @Query("firstAidKit") String firstAidKit,
            @Query("accidentInformation") String accidentInformation,
            @Query("liquidsLevelCheck") String liquidsLevelCheck,
            @Query("radiator") String radiator,
            @Query("oil") String oil,
            @Query("autoTransmission") String autoTransmission,
            @Query("powerSteering") String powerSteering,
            @Query("brakes") String brakes,
            @Query("windowWasher") String windowWasher,
            @Query("driverId") String driverId,
            @Query("damageSection") String damageSection,
            @Query("damageDesc") String damageDesc
            );
}

