package com.vamosys.drs.driver.Activity;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vamosys.drs.driver.AppPreference;
import com.vamosys.drs.driver.R;

public class Lang_Row extends RecyclerView.Adapter<Lang_Row.ViewHolder> {
Context ctx;
int i;
    public Lang_Row(Context applicationContext, int i) {
        this.ctx=applicationContext;
        this.i=i;
    }

    @NonNull
    @Override
    public Lang_Row.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.lang_row,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final Lang_Row.ViewHolder holder, int position) {

if (holder.appPreference.getImgCheck().equalsIgnoreCase("eng")){
    holder.img_eng.setVisibility(View.VISIBLE);

}
        System.out.println("grfg"+holder.appPreference.getImgCheck());

        if (holder.appPreference.getImgCheck().equalsIgnoreCase("hin")){
            holder.img_hin.setVisibility(View.VISIBLE);
        }


       holder. rl_malayalam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.img_mal.setVisibility(View.VISIBLE);
                holder .img_hin.setVisibility(View.GONE);
                holder.img_tam.setVisibility(View.GONE);
                holder.img_tel.setVisibility(View.GONE);
                holder.img_eng.setVisibility(View.GONE);

            }
        });
       holder. rl_telugu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder. img_tel.setVisibility(View.VISIBLE);
                holder .img_hin.setVisibility(View.GONE);
                holder.img_tam.setVisibility(View.GONE);
                holder.img_mal.setVisibility(View.GONE);
                holder.img_eng.setVisibility(View.GONE);

            }
        });
       holder. rl_tamil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.img_tam.setVisibility(View.VISIBLE);
                holder.img_hin.setVisibility(View.GONE);
                holder.img_mal.setVisibility(View.GONE);
                holder.img_tel.setVisibility(View.GONE);
                holder.img_eng.setVisibility(View.GONE);
            }
        });

        holder. rl_english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.img_eng.setVisibility(View.VISIBLE);
                holder.img_hin.setVisibility(View.GONE);
                holder.img_tam.setVisibility(View.GONE);
                holder.img_tel.setVisibility(View.GONE);
                holder.img_mal.setVisibility(View.GONE);
                holder.appPreference.setLangName("english");
                holder.appPreference.setImgCheck("eng");
                Intent i=new Intent(ctx,SigninActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(i);
            }
        });

        holder. rl_hindi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.img_hin.setVisibility(View.VISIBLE);
                holder.img_mal.setVisibility(View.GONE);
                holder.img_tam.setVisibility(View.GONE);
                holder.img_tel.setVisibility(View.GONE);
                holder.img_eng.setVisibility(View.GONE);
               // holder.setLanguage("hi");
                holder.appPreference.setLangName("hindi");
                holder.appPreference.setImgCheck("hin");
                Intent i=new Intent(ctx,SigninActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return i;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout rl_hindi, rl_tamil, rl_telugu, rl_malayalam,rl_english;
        ImageView img_hin, img_tam, img_tel, img_mal,img_eng;
        AppPreference appPreference;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            rl_hindi = itemView.findViewById(R.id.rl_hindi);
            rl_english=itemView.findViewById(R.id.rl_english);
            rl_tamil = itemView.findViewById(R.id.rl_tamil);
            rl_telugu = itemView.findViewById(R.id.rl_telugu);
            rl_malayalam = itemView.findViewById(R.id.rl_malayalam);
            img_hin =itemView. findViewById(R.id.img_hin);
            img_mal = itemView.findViewById(R.id.img_mal);
            img_tam = itemView.findViewById(R.id.img_tam);
            img_tel =itemView. findViewById(R.id.img_tel);
            img_eng=itemView.findViewById(R.id.img_eng);
            appPreference= new AppPreference(ctx);
        }
    }
}
