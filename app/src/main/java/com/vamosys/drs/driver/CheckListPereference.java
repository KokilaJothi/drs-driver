package com.vamosys.drs.driver;

import android.content.Context;
import android.content.SharedPreferences;

public class CheckListPereference {
    private SharedPreferences mSharedPreferences;
    private static final String sateft_belt="sateft_belt";
    private static final String brake="brake";
    private static final String engine="engine";
    private static final String message="message";
    private static final String transmission="transmission";
    private static final String greecePacking="greece";
    private static final String wipers="wipers";
    private static final String highbeam="highbeam";
    private static final String lowbeam="lowbeam";
    private static final String turnSignal="turnSignal";
    private static final String breakLight="breakLight";
    private static final String doors="doors";
    private static final String windows="windows";
    private static final String radio="radio";
    private static final String horn="horn";
    private static final String tires="tires";

    private static final String isCheckListAdded="isCheckListAdded";

    public CheckListPereference(Context context) {
        mSharedPreferences = context.getSharedPreferences("driver_checkList", Context.MODE_PRIVATE);
    }
    public void setIsCheckListAdded(Boolean name) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putBoolean(isCheckListAdded, name);
        edit.commit();
    }

    public Boolean getIsCheckListAdded() {
        return mSharedPreferences.getBoolean(isCheckListAdded,false);
    }
    public void setSafetyBelt(String name) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(sateft_belt, name);
        edit.commit();
    }

    public String getSateft_belt() {
       return mSharedPreferences.getString(sateft_belt,"OK:-");
    }
    public void setBrake(String name) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(brake, name);
        edit.commit();
    }

    public String getBrake() {
        return mSharedPreferences.getString(brake,"OK:-");
    }
    public void setEngine(String name) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(engine, name);
        edit.commit();
    }

    public String getEngine() {
        return mSharedPreferences.getString(engine,"OK:-");
    }
    public void setMessage(String name) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(message, name);
        edit.commit();
    }

    public String getMessage() {
        return mSharedPreferences.getString(message,"");
    }
    public void setTransmission(String name) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(transmission, name);
        edit.commit();
    }

    public String getTransmission() {
        return mSharedPreferences.getString(transmission,"OK:-");
    }
    public void setGreecePacking(String name) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(greecePacking, name);
        edit.commit();
    }

    public String getGreecePacking() {
        return mSharedPreferences.getString(greecePacking,"OK:-");
    }
    public void setWipers(String name) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(wipers, name);
        edit.commit();
    }

    public String getWipers() {
        return mSharedPreferences.getString(wipers,"OK:-");
    }
    public void setHighbeam(String name) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(highbeam, name);
        edit.commit();
    }

    public String getHighbeam() {
        return mSharedPreferences.getString(highbeam,"OK:-");
    }
    public void setLowbeam(String name) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(lowbeam, name);
        edit.commit();
    }

    public String getLowbeam() {
        return mSharedPreferences.getString(lowbeam,"OK:-");
    }
    public void setTurnSignal(String name) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(turnSignal, name);
        edit.commit();
    }

    public String getTurnSignal() {
        return mSharedPreferences.getString(turnSignal,"OK:-");
    }
    public void setBreakLight(String name) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(breakLight, name);
        edit.commit();
    }

    public String getBreakLight() {
        return mSharedPreferences.getString(breakLight,"OK:-");
    }
    public void setDoors(String name) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(doors, name);
        edit.commit();
    }

    public String getDoors() {
        return mSharedPreferences.getString(doors,"OK:-");
    }
    public void setWindows(String name) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(windows, name);
        edit.commit();
    }

    public String getWindows() {
        return mSharedPreferences.getString(windows,"OK:-");
    }
    public void setRadio(String name) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(radio, name);
        edit.commit();
    }

    public String getRadio() {
        return mSharedPreferences.getString(radio,"OK:-");
    }
    public void setHorn(String name) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(horn, name);
        edit.commit();
    }

    public String getHorn() {
        return mSharedPreferences.getString(horn,"OK:-");
    }
    public void setTires(String name) {
        SharedPreferences.Editor edit = mSharedPreferences.edit();
        edit.putString(tires, name);
        edit.commit();
    }

    public String getTires() {
        return mSharedPreferences.getString(tires,"OK:-");
    }
}
