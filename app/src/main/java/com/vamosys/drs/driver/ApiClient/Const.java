package com.vamosys.drs.driver.ApiClient;

import com.vamosys.drs.driver.Response.SpinnerHub;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Const {

    public static String UUID;
    public static boolean isregister = false;
    public static String Mobile_number;
    public static String isexit;
    public static Boolean UserExists=false;

    public static String VehicleId;
    public static String driverId;
    public static List<SpinnerHub> list;

    public static String convertMonthComa(long dateTime){
        Date date = new Date(dateTime); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd @ hh:mm aa"); // the format of your date
        String formattedDate = sdf.format(date);
        return formattedDate;
    }
}


