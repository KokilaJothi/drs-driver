package com.vamosys.drs.driver.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.vamosys.drs.driver.R;

public class HomeFragament2 extends Fragment {
    BottomSheetDialog mBottomSheetDialog;
    LinearLayout lay_lod_strt,lay_lod_com,lay_unld_srt,lay_unld_com;
    ImageView close_img,img_lod_strt,img_lod_com,img_unld_srt,img_unld_com;
    CardView card_chnVehicle;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View homeLayout= inflater.inflate(R.layout.fragment_home2, container, false);
        card_chnVehicle=homeLayout.findViewById(R.id.card_changevehicle);
        mBottomSheetDialog = new BottomSheetDialog(getContext());
        View sheetView = getLayoutInflater().inflate(R.layout.loading_status_bottom_dialog, null);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.setCancelable(false);
        lay_lod_strt=sheetView.findViewById(R.id.lay_load_start);
        lay_lod_com=sheetView.findViewById(R.id.lay_lod_comp);
        lay_unld_srt=sheetView.findViewById(R.id.lay_unlod_start);
        lay_unld_com=sheetView.findViewById(R.id.lay_unlod_completed);
        img_lod_strt=sheetView.findViewById(R.id.img_lod_strt);
        img_lod_com=sheetView.findViewById(R.id.img_lod_com);
        img_unld_srt=sheetView.findViewById(R.id.img_unld_srt);
        img_unld_com=sheetView.findViewById(R.id.img_unld_com);
        close_img=sheetView.findViewById(R.id.img_close);
        mBottomSheetDialog.setContentView(sheetView);
        ((View) sheetView.getParent()).setBackgroundColor(HomeFragament2.this.getResources().getColor(android.R.color.transparent));
        card_chnVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.show();
            }
        });
        lay_lod_strt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_lod_strt.setImageDrawable(getActivity().getDrawable(R.drawable.red_tic));
                img_unld_srt.setImageDrawable(getActivity().getDrawable(R.drawable.circle_24dp));
                img_lod_com.setImageDrawable(getActivity().getDrawable(R.drawable.circle_24dp));
                img_unld_com.setImageDrawable(getActivity().getDrawable(R.drawable.circle_24dp));
            }
        });
        lay_lod_com.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_lod_com.setImageDrawable(getActivity().getDrawable(R.drawable.red_tic));
                img_lod_strt.setImageDrawable(getActivity().getDrawable(R.drawable.circle_24dp));
                img_unld_srt.setImageDrawable(getActivity().getDrawable(R.drawable.circle_24dp));
                img_unld_com.setImageDrawable(getActivity().getDrawable(R.drawable.circle_24dp));

            }
        });
        lay_unld_srt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_unld_srt.setImageDrawable(getActivity().getDrawable(R.drawable.red_tic));
                img_lod_com.setImageDrawable(getActivity().getDrawable(R.drawable.circle_24dp));
                img_lod_strt.setImageDrawable(getActivity().getDrawable(R.drawable.circle_24dp));
                img_unld_com.setImageDrawable(getActivity().getDrawable(R.drawable.circle_24dp));
            }
        });
        lay_unld_com.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_unld_com.setImageDrawable(getActivity().getDrawable(R.drawable.red_tic));
                img_unld_srt.setImageDrawable(getActivity().getDrawable(R.drawable.circle_24dp));
                img_lod_com.setImageDrawable(getActivity().getDrawable(R.drawable.circle_24dp));
                img_lod_strt.setImageDrawable(getActivity().getDrawable(R.drawable.circle_24dp));
            }
        });
        close_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });
        return homeLayout;
    }
}
